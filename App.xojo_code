#tag Class
Protected Class App
Inherits Application
	#tag Event
		Sub Close()
		  // Release our lock on the mutex only if we're the original program
		  if mMutex.TryEnter then
		    mMutex.Leave
		    mMutex.Leave
		  end
		  
		  // We provide this back-door way to clear our preferences before
		  // saving them, so we can conveniently test them on the next run.
		  // If this conficts with any keyboard modifiers you want to offer,
		  // change or delete it as you see fit.
		  if Keyboard.AsyncAltKey then
		    Prefs.Delete
		    MsgBox "Preference file has been deleted."
		  else
		    if Keyboard.AsyncShiftKey then
		      Prefs.Clear
		      msgBox "Preference settings have been cleared."
		    end if
		    // We'd love to save our prefs in its destructor,
		    // so that we wouldn't have to worry about it here.
		    // However, it doesn't seem to get called when the
		    // app is shutting down...perhaps at App.Close, all
		    // module objects (which is what our prefs object
		    // currently is) are already deleted?
		    //
		    // So we do this in here for now...
		    Prefs.Save
		  end if
		  
		  
		  //Last, we need to clear the user lastTouch timer to allow program restart
		  //This should not happen if the program is quitting because of an early error or
		  // because the user is logged in somewhere else.  This would be a "ForceQuit"
		  
		  if not forcingQuit then
		    if myDB.Connect then
		      myDB.SQLExecute ("UPDATE User SET LastTouched=Null WHERE Username='"+ CurrentUser.Username +"'")
		    end // No error checking or handling, because this isn't critical for most runs.
		  end
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  // Create the mutex with my application's name
		  mMutex = new Mutex(AppName)
		  // Now try to enter the mutex
		  if  mMutex.TryEnter then
		  else
		    ForceQuit
		  end if
		  // At this point, we own the lock on the mutex; we're the first app
		  
		  
		  dim f As FolderItem
		  Prefs.Get("DataBase",f)
		  if f=nil or not f.Exists then
		    f= new FolderItem
		    f=f.Child("Data")
		    if f.Exists then f=f.Child ("Report Tracker.sqlite")
		    if f=nil or not f.Exists then
		      if f<>nil then MsgBox ("Can't Find the database file "+f.NativePath+EndOfLine+EndOfLine+f.URLPath)
		      Dim dlg as OpenDialog
		      dlg= New OpenDialog
		      f=new FolderItem
		      dlg.InitialDirectory= f
		      dlg.Title="Find Database File"
		      dlg.PromptText="Please locate the Tracker Database File:"
		      dlg.Filter=databaseFileTypes.All
		      f=dlg.ShowModal()
		      If f = Nil then
		        MsgBox "Can't continue."+EndOfLine+EndOfLine+"Please contact your system administrator."
		        ForceQuit
		      End if
		    end
		  end
		  
		  // We have an apparently valid FolderItem for the database.  Test it.
		  myDB= new ReportTrackingDB
		  
		  myDB.DatabaseFile = f
		  myDB.MultiUser = true
		  
		  If not myDB.Connect() then
		    Beep
		    MsgBox "The Tracker Database couldn't be opened."+EndOfLine+EndOfLine+"Please contact your system administrator"
		    ForceQuit
		  end if
		  Prefs.Set("DataBase",f)
		  
		  // We now have a gook link to Database. Check Version info and Load Environment Preferences, Officer ID list, and needed tables info.
		  dim rs As RecordSet
		  rs=myDB.SQLSelect ("SELECT ID, Lastname, Firstname FROM User WHERE Active='True'")
		  if myDB.Error then
		    Beep
		    MsgBox "Can't load the Officer ID list." +  EndOfLine + EndOfLine +"Please contact your system administrator ("+_
		    myDB.ErrorMessage+", "+str(myDB.ErrorCode)+ ")"
		    ForceQuit
		  end
		  
		  Officers = new Dictionary
		  dim j As Integer = rs.RecordCount
		  rs.MoveFirst
		  for i as integer = 1 to j
		    Officers.Value(rs.Field("ID").StringValue) = rs.Field("LastName").StringValue+", "+rs.Field("FirstName").StringValue
		    rs.MoveNext
		  next
		  
		  dim h As FolderItem=f.Parent
		  h=h.Child("Error_Logs")
		  if h<>nil then
		    if not h.Exists then h.CreateAsFolder  //Creates log folder, if none
		    DLogRootFolder=h
		  end
		  
		  rs=myDB.SQLSelect ("SELECT * from Environment;")
		  if not myDB.Error and rs<>nil then
		    j=rs.RecordCount
		    rs.MoveFirst
		    for i as integer = 1 to j
		      prefs.Set(rs.Field("Name").StringValue, rs.Field("Value").StringValue)
		      rs.MoveNext
		    next
		    
		    
		  end
		  myDB.Close
		  
		  
		  CurrentUser = new Profile
		  dim d As new date
		  CurrentYear = d.Year-2000
		  
		  // create Shortcut on Desktop, if Environment set
		  #if TargetWin32 then
		    dim b As Boolean  = False
		    prefs.get("Build Shortcut", b) //will only build if environment explicitly has a true value
		    if b then
		      dim foundit as Boolean = False
		      dim home As Folderitem = app.ExecutableFile
		      dim dtop As FolderItem =SpecialFolder.Desktop
		      if home = nil or dtop = nil then return
		      dim c As integer =dtop.Count
		      
		      dim rf As FolderItem ' f is the resolved file destinations
		      dim tf As FolderItem 'tf points to the actual shortcut file
		      
		      for i as integer = 1 to c
		        rf=dtop.TrueItem(i)
		        if rf<>nil then
		          if rf.Alias then
		            tf=dtop.Item(i)
		            if tf<> nil and tf.exists and tf.ShellPath=home.ShellPath then
		              foundit = true
		              exit
		            end
		          end
		        end
		      next
		      
		      If foundit then Return
		      
		      // Not found on desktop.  Make one.
		      rf = CreateShortcut(home, "Report Tracker" )
		      if rf<> nil and rf.Exists then
		        rf.MoveFileTo (SpecialFolder.Desktop)
		      End
		    end //BuildShortcut
		  #endif
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function CreateShortcut(scTarget as FolderItem, scName as String) As FolderItem
		  //Creates a shortcut (.lnk file) in the users %TEMP% directory named scName and pointing to scTarget. Returns
		  //a FolderItem corresponding to the shortcut file. You must move the returned Shortcut file to the desired directory.
		  //On error, returns Nil.
		  
		  #If TargetWin32 Then
		    Dim lnkObj As OLEObject
		    Dim scriptShell As New OLEObject("{F935DC22-1CF0-11D0-ADB9-00C04FD58A0B}")
		    
		    If scriptShell <> Nil then
		      lnkObj = scriptShell.CreateShortcut(SpecialFolder.Temporary.AbsolutePath + scName + ".lnk")
		      If lnkObj <> Nil then
		        lnkObj.Description = scName
		        lnkObj.TargetPath = scTarget.AbsolutePath
		        lnkObj.WorkingDirectory = scTarget.AbsolutePath
		        lnkObj.Save
		        Return SpecialFolder.Temporary.TrueChild(scName + ".lnk")
		      Else
		        Return Nil
		      End If
		    Else
		      Return Nil
		    End If
		  #endif
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ForceQuit()
		  forcingQuit = true
		  Quit
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetWindowByName(Title as String) As Window
		  // Given Title, cycle through all open windows and if there's a match return it.
		  // No match returns nil
		  
		  dim w As Window
		  
		  For i as integer = WindowCount -1 DownTo 0
		    w=Window(i)
		    if W <> nil and w.Title = Title then
		      Return w
		      Exit
		    end
		  Next
		  
		  Return Nil
		End Function
	#tag EndMethod


	#tag Note, Name = To-Do's
		Put in Program Updater. App.Open
		
		Do search Logic
		
		centralize logic into db.class
	#tag EndNote


	#tag Property, Flags = &h0
		CurrentUser As Profile
	#tag EndProperty

	#tag Property, Flags = &h0
		CurrentYear As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private forcingQuit As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMutex As mutex
	#tag EndProperty

	#tag Property, Flags = &h0
		myDB As ReportTrackingDB
	#tag EndProperty

	#tag Property, Flags = &h0
		Officers As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		YearTables() As Integer
	#tag EndProperty


	#tag Constant, Name = kEditClear, Type = String, Dynamic = False, Default = \"&Delete", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"&Delete"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"&Delete"
	#tag EndConstant

	#tag Constant, Name = kFileQuit, Type = String, Dynamic = False, Default = \"&Quit", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"E&xit"
	#tag EndConstant

	#tag Constant, Name = kFileQuitShortcut, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Mac OS, Language = Default, Definition  = \"Cmd+Q"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"Ctrl+Q"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="CurrentYear"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
