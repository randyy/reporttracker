#tag Class
Protected Class NamedStrings
	#tag Method, Flags = &h0
		Sub Clear()
		  
		  // Clear all existing preference info.
		  
		  _Items = nil
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Clear(name as string)
		  
		  // Clear the indicated preference value.
		  
		  Set(name, "")
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(file as FolderItem = nil)
		  // Clear existing preferences before reading anything.
		  
		  Clear
		  
		  //  *************************************************************
		  //  **  In this mod, I load the base pref, if any, then override  **
		  //  **  with the preferences folder.                                          **
		  //  *************************************************************
		  
		  Read(file)
		  
		  dim master As new FolderItem
		  dim maxLength As Integer=255
		  dim FileName as string = AppName + " Preferences"
		  
		  if FileName.Len > maxLength then
		    FileName = AppName + " Prefs"
		  end if
		  if FileName.Len > maxLength then
		    FileName = AppName
		  end if
		  FileName = FileName.Left(maxLength)
		  master=master.Child(FileName)
		  if Master <> nil then read(master)
		  
		  LastFileUsed=file  // added to reset save location to personal Folder
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Delete(file as folderitem = nil)
		  if file = nil then
		    file = LastFileUsed // use what we used last time
		  else
		    LastFileUsed = file // remember for later
		  end if
		  
		  if file <> nil then
		    
		    // Here we delete our preference file.
		    file.Delete
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Get(name as string, byref t as boolean)
		  
		  // Boolean preferences take a little thought:
		  //
		  //   name is present, value =  "true"  ->  t = true
		  //   name is present, value <> "true"  ->  t = false
		  //   name is not present               ->  t is unchanged
		  
		  dim s as string = Value(name)
		  if s <> "" then
		    t = (s = "true")
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Get(name as string, byref c as color)
		  
		  // Color preferences take a little thought:
		  //
		  //   name is present, value is parseable  ->  c = value
		  //   name is present, value not parseable ->  c is unchanged
		  //   name not present                     ->  c is unchanged
		  
		  dim positions(6) as integer
		  dim i, iMax as integer
		  dim r, g, b as integer
		  dim failed as boolean
		  
		  dim s as string = Value(name)
		  
		  // Make sure we have exactly six legal hex digits,
		  // While we do so, record each digit's position in
		  // our hex digit string. (This is why we don't use
		  // variants for this -- the formatting is strange,
		  // and the variant conversion can't tell black from
		  // a nonsensical entry.
		  
		  iMax = s.Len
		  if iMax = 6 then
		    
		    for i = 1 to iMax // note: 1-based
		      
		      positions(i) = HexDigits.InStr(s.Mid(i, 1)) - 1
		      
		      // Early exit, if we find a non-hex digit.
		      
		      if positions(i) < 0 then
		        failed = true
		        exit
		      end if
		      
		    next i
		    
		    if not failed then
		      
		      r = positions(1) * 16 + positions(2)
		      g = positions(3) * 16 + positions(4)
		      b = positions(5) * 16 + positions(6)
		      
		      c = RGB(r, g, b)
		      
		    end if
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Get(name as string, byref numbers() as double)
		  
		  // This gets an array of numbers of the indicated type.
		  
		  dim values() as string
		  Get(name, values())
		  
		  dim i as integer
		  if UBound(values) >= 0 then
		    
		    redim numbers(UBound(values))
		    
		    for i = 0 to UBound(numbers)
		      numbers(i) = val(values(i))
		    next i
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Get(name as string, byref number as double)
		  
		  // This gets a single number of the indicated type.
		  // If there are multiple numbers on a line but only
		  // one number is asked for, the result is undefined.
		  // (Most likely, you will get the first number.)
		  
		  dim s as string = Value(name)
		  if s <> "" then
		    number = val(s)
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Get(name as string, byref f as folderItem)
		  
		  // If the preference value is an empty string,
		  // don't create a folderItem from it. We'd just
		  // get the current directory, which is not what
		  // the absence of a preference value means for
		  // folder items -- it means there is no stored
		  // folderItem; i.e., don't touch our input.
		  
		  // Also, since we store folderItem strings as Base64
		  // encoded, we must decode them on the way out.
		  
		  dim s as string = Value(name)
		  if s <> "" then
		    s = DecodeBase64(s)
		    f = GetFolderItem(s) // using "new FolderItem" in 5.5a11 with a nonexistent path causes RB assertion
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Get(name as string, byref numbers() as integer)
		  
		  // This gets an array of numbers of the indicated type.
		  
		  dim values() as string
		  Get(name, values())
		  
		  dim i as integer
		  if UBound(values) >= 0 then
		    
		    redim numbers(UBound(values))
		    
		    for i = 0 to UBound(numbers)
		      numbers(i) = val(values(i))
		    next i
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Get(name as string, byref number as integer)
		  
		  // This gets a single number of the indicated type.
		  // If there are multiple numbers on a line but only
		  // one number is asked for, the result is undefined.
		  // (Most likely, you will get the first number.)
		  
		  dim s as string = Value(name)
		  if s <> "" then
		    number = val(s)
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Get(name as string, byref strings() as string)
		  
		  // This gets a list of strings. It can be used
		  // on its own, but it is also used by variations
		  // of Get for other multiple-value situations.
		  //
		  // Note: quotes, brackets and other attempts to embed
		  // a separator in a string are not supported yet.
		  
		  dim i as integer
		  dim values() as string
		  
		  dim s as string = Value(name)
		  
		  if s <> "" then
		    strings = s.Split(Separator)
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Get(name as string, byref s as string)
		  
		  // If this name is in our list of items, then adjust
		  // our byref argument to its value. Otherwise, leave
		  // our byref argument alone.
		  //
		  
		  if Items.HasKey(name) then
		    s = Items.Value(name)
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetWindowPos(windowName as string, win as MDIWindow)
		  
		  dim values() as integer
		  
		  if win <> nil then
		    
		    Get(windowName + Suffix_WindowPos, values)
		    
		    if UBound(values) >= 0 then
		      win.Left = values(0)
		    end if
		    
		    if UBound(values) >= 1 then
		      win.Top = values(1)
		    end if
		    
		    if UBound(values) >= 2 then
		      win.Width = values(2)
		    end if
		    
		    if UBound(values) >= 3 then
		      win.Height = values(3)
		    end if
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetWindowPos(windowName as string, win as Window)
		  dim t,l,w,h as integer
		  dim values() as integer
		  dim inscreen As Boolean = false
		  
		  if win = nil then return
		  Get(windowName + Suffix_WindowPos, values)
		  if UBound(Values)<3 then return //a valid window has 4 fields (left, top, width, height)
		  
		  // store old values in case we need to revert (the new values are no longer on screen due to resolution change
		  t=win.Top
		  l=win.Left
		  h=win.Height
		  w=win.Width
		  
		  // position window
		  win.Left = values(0)
		  win.Top = values(1)
		  // If the window has a grow icon, then auto-adjust
		  // its size as well as its position. Otherwise leave
		  // it alone, on the theory that if the size has changed
		  // (say, with a new version of the program), then the
		  // old preferences shouldn't apply to non-resizeable
		  // windows.
		  if win.Resizeable then
		    win.Width = min(max(values(2),win.MinWidth),win.MaxWidth)
		    win.Height = min(max(values(3),win.MinHeight),win.MaxHeight)
		  end if
		  
		  
		  // Now, check to see if a usable portion of the titlebar is on a screen
		  // We want 22 pixels vertical and at least 40 horizontal.
		  for i as Integer= 0 to ScreenCount-1
		    dim n As integer
		    if win.Top<(22+Screen(i).AvailableTop+screen(i).top) then Continue //top 22 more than topavail
		    if win.Top>(Screen(i).Top+Screen(i).AvailableTop+Screen(i).AvailableHeight) then Continue //win.top doesn't include titlebar
		    if win.Left>(Screen(i).Left+Screen(i).AvailableLeft+Screen(i).AvailableWidth-40) then Continue
		    if (win.left+win.Width)<(Screen(i).Left+Screen(i).AvailableLeft+40) then Continue
		    // Window passed all tests
		    inscreen = true
		    exit
		  next
		  
		  if inscreen then return
		  // reset to old values since it failed tests
		  win.Left = l
		  win.Top = t
		  win.Height = h
		  win.Width = w
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Items() As Dictionary
		  
		  if _Items = nil then _Items = new Dictionary
		  return _Items
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Read(file as folderItem)
		  
		  // Get our preferences folder item and read its contents. If
		  // there's no such item, don't throw an error, just return.
		  
		  
		  
		  dim stream as textInputStream
		  dim content, lines(), line, name, value as string
		  dim valueLen as integer
		  
		  
		  LastFileUsed = file // remember for later
		  
		  if file <> nil then
		    
		    // Here we read our preferences from a text file.
		    // But it could be a binary file, or even a folder
		    // into which other items are put. It's up to you.
		    
		    stream = file.OpenAsTextFile
		    if stream <> nil then
		      
		      // OK, now we're ready to actually read the file.
		      // On each line, the preference name is the text
		      // before the first separator, and the value is all
		      // the text after that -- even if it contains more
		      // separators. (This means we can store a list of
		      // values as a single preference.)
		      
		      content = stream.ReadAll
		      stream.close
		      
		      lines = ReplaceLineEndings(content, EndOfLine).Split(EndOfLine)
		      
		      for each line in lines
		        
		        name = line.NthField(Separator, 1)
		        
		        if name.Len > 0 then
		          
		          valueLen = line.Len - name.Len - Separator.Len
		          value = Right(line, valueLen)
		          
		          Set(name, value.Split(Separator))
		          
		        end if
		        
		      next line
		      
		    end if
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Save(file as folderItem = nil)
		  
		  // Get our preferences folder item and write its contents.
		  
		  dim stream as textOutputStream
		  dim s as string
		  dim i, iMax as integer
		  
		  if file = nil then
		    file = LastFileUsed // use what we used last time
		  else
		    LastFileUsed = file // remember for later
		  end if
		  
		  if file <> nil then
		    
		    // Here we write our preferences into a text file.
		    
		    stream = file.CreateTextFile
		    if stream = nil then
		      
		      Beep
		      MsgBox "Can't create the text file """ _
		      + file.AbsolutePath + """"
		      
		    else // here if the prefs file was created
		      
		      // OK, now we're ready to actually write the file.
		      
		      iMax = Items.Count-1
		      for i = 0 to iMax
		        
		        s = Items.Key(i)
		        stream.WriteLine s + Separator + Items.Value(s)
		        
		      next i
		      
		      stream.close
		      
		    end if
		    
		    // If we're on the Mac, adjust the type and creator
		    // such that our preferred application will open it
		    // when it's double-clicked.
		    
		    'file.MacType = MacType_Text
		    'file.MacCreator = MacCreator_TextWrangler
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Separator() As string
		  
		  // This separates values from names, and also from
		  // other values in cases where we store multiple
		  // values per name.
		  //
		  // A tab character is chosen because it is compatible
		  // with multiple filenames and multiple numbers on a
		  // single line. It does limit string parameters to
		  // one value per name. (This might relax someday if
		  // we update to respect quotes or some such thing.)
		  
		  return TAB
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(name as string, t as boolean)
		  
		  // This accepts a boolean.
		  
		  if t then
		    Set(name, "true")
		  else
		    Set(name, "") // false values don't get saved explicitly
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(name as string, c as color)
		  
		  // This accepts a color.
		  
		  // We write colors in the form "RRGGBB",
		  // where all digits are hex.
		  
		  dim r as integer = c.Red
		  dim g as integer = c.Green
		  dim b as integer = c.Blue
		  
		  dim s as string
		  
		  s = s + HexDigits.Mid((r  /  16) + 1, 1)
		  s = s + HexDigits.Mid((r mod 16) + 1, 1)
		  
		  s = s + HexDigits.Mid((g  /  16) + 1, 1)
		  s = s + HexDigits.Mid((g mod 16) + 1, 1)
		  
		  s = s + HexDigits.Mid((b  /  16) + 1, 1)
		  s = s + HexDigits.Mid((b mod 16) + 1, 1)
		  
		  Set(name, s)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(name as string, numbers() as double)
		  
		  // This accepts an array of doubles. There are
		  // other routines that convert arrays of other
		  // numerical types into doubles.
		  //
		  // Example:
		  //
		  //   prefs.Set("powersOfTen", Array(0.1, 1.0, 10.0, 100.0))
		  //   prefs.Set("powersOfTwo", Array(0, 1, 2, 3))
		  
		  dim strings() as string
		  
		  dim i as integer
		  for i = 0 to UBound(numbers)
		    
		    strings.Append str(numbers(i))
		    
		  next i
		  
		  Set(name, strings)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(name as string, number as double)
		  
		  // This accepts a single value of any numerical type.
		  
		  Set(name, str(number))
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(name as string, f as folderItem)
		  
		  // This accepts a folderItem (which may be nil).
		  
		  dim s as string
		  dim i, iMax as integer
		  
		  // We translate a folderItem into a Base-64-encoded
		  // string with no line breaks, suitable for storage
		  // as a preference value.
		  //
		  // Since GetSaveInfo returns binary data in string form,
		  // we might have binary characters (line feeds, returns)
		  // that interfere with our text format. So before writing
		  // it out, we encode it as Base64, which transforms all
		  // unprintable binary characters into runs of printable
		  // ASCII characters.
		  
		  if f <> nil then
		    
		    s = f.GetSaveInfo(nil)
		    s = EncodeBase64(s, 0) // 0 = no extra line breaks
		    s = ReplaceLineEndings(s, "")
		    
		  end if
		  
		  Set(name, s) // even if f is nil
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(name as string, numbers() as integer)
		  
		  // This just feeds a different numerical array
		  // type into our double-array setting method.
		  
		  dim doubles() as double
		  
		  dim i as integer
		  for i = 0 to UBound(numbers)
		    doubles.Append numbers(i)
		  next i
		  
		  Set(name, doubles)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(name as string, numbers() as single)
		  
		  // This just feeds a different numerical array
		  // type into our double-array setting method.
		  
		  dim doubles() as double
		  
		  dim i as integer
		  for i = 0 to UBound(numbers)
		    doubles.Append numbers(i)
		  next i
		  
		  Set(name, doubles)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(name as string, strings() as string)
		  
		  // This is the main method for associating a
		  // name with a string value (or set of values).
		  //
		  // If our value is null, then we remove this
		  // name from our list of names. Otherwise, we
		  // associate this name with the given set of
		  // string values, destroying any previous
		  // association of this name.
		  //
		  // What is a null value? If there are no strings,
		  // or there is one string and it is empty, then
		  // that is a null value. (If there are multiple
		  // strings, we allow any of them to be empty.)
		  //
		  // If any input string contains our separator
		  // or a CR character, then an exception will be
		  // raised and our list of names will be unchanged.
		  
		  dim s as string
		  dim i as integer
		  dim err as new RuntimeException
		  
		  
		  if UBound(strings) = -1 _
		    or (UBound(strings) = 0 and strings(0).Len = 0) then
		    
		    // It's a null value, remove the name.
		    
		    if Items.HasKey(name) then
		      Items.Remove(name)
		    end if
		    
		  else
		    
		    // Inspect all strings for special delimiter values.
		    // If none is found, update the value for this name.
		    
		    for i = 0 to UBound(strings)
		      
		      if strings(i).InStr(Separator) > 0 then
		        break
		        'RaiseException ("NamedStrings.Set: Separator found in """ + name + """ string")
		      elseif strings(i).InStr(CR) > 0 then
		        break
		        'RaiseException ("NamedStrings.Set: CR found in """ + name + """ string")
		      else
		        // Accumulate this value into a running
		        // string, with a separator if it's not
		        // the first value.
		        
		        if i > 0 then
		          s = s + Separator
		        end if
		        
		        s = s + strings(i)
		        
		      end if
		      
		    next i
		    
		    Items.Value(name) = s
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Set(name as string, value as string)
		  
		  // This accepts a single string.
		  
		  Set(name, Array(value))
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetWindowPos(windowName as string, win as MDIWindow)
		  
		  if win <> nil then
		    
		    Set(windowName + Suffix_WindowPos, _
		    Array(win.Left, win.Top, win.Width, win.Height))
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetWindowPos(windowName as string, win as Window)
		  
		  if win <> nil then
		    
		    Set(windowName + Suffix_WindowPos, _
		    Array(win.Left, win.Top, win.Width, win.Height))
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Value(name as string) As string
		  
		  // If this name is in our list of items, then return
		  // its value. Otherwise, return an empty string.
		  //
		  // (This is the main value-retrieval method;
		  // the others all call this one.)
		  
		  if Items.HasKey(name) then
		    return Items.Value(name)
		  end if
		  
		  return ""
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private LastFileUsed As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private _Items As Dictionary
	#tag EndProperty


	#tag Constant, Name = HexDigits, Type = String, Dynamic = False, Default = \"0123456789ABCDEF", Scope = Private
	#tag EndConstant

	#tag Constant, Name = Suffix_WindowPos, Type = String, Dynamic = False, Default = \"_LeftTopWidthHeight", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
