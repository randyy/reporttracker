#tag Module
Protected Module Preferences
	#tag Method, Flags = &h21
		Private Function PreferencesFile(ApplicationName as string) As folderItem
		  
		  // Returns a folderItem representing a file within the
		  // preferences folder that is appropriate for the given
		  // application name.
		  
		  if ApplicationName = "" then
		    MsgBox "Error - Preferences.File was called with an empty application name."
		  end if
		  
		  // Shrink the file name as appropriate to the platform.
		  
		  dim maxLength as integer = 255
		  dim f,g As FolderItem
		  
		  #if TargetPPC or Target68K
		    maxLength = 31 // OS 9 only allows 31 chars
		  #endif
		  
		  dim FileName as string = ApplicationName + " Preferences"
		  
		  if FileName.Len > maxLength then
		    FileName = ApplicationName + " Prefs"
		  end if
		  
		  if FileName.Len > maxLength then
		    FileName = ApplicationName
		  end if
		  
		  FileName = FileName.Left(maxLength)
		  
		  f=SpecialFolder.Preferences.Child(FileName)
		  if not f.Exists then
		    g=new FolderItem
		    g=g.Child(FileName)
		    if g.Exists then g.CopyFileTo f
		  end
		  return f
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Prefs() As NamedStrings
		  
		  // This routine returns the object that stores, and
		  // provides an API to, our preference names and values.
		  
		  // If we don't have a preferences object, make one.
		  // (This is so that our caller doesn't have to worry
		  // about it -- he just calls this routine.)
		  
		  if _Prefs = nil then
		    _Prefs = new NamedStrings(PreferencesFile(AppName))
		  end if
		  
		  return _Prefs
		  
		End Function
	#tag EndMethod


	#tag Note, Name = About this module
		
		This module takes care of Preferences handling. To use it, 
		call Prefs.Get/Set or one of their variations. (Looking at
		how they're called elsewhere is the easiest way to see how 
		they work.)
		
		To re-use this module in another project, just drag the 
		Preferences folder to the Macintosh Finder or the Windows 
		Explorer, and from there into your project. You'll have to 
		change "AppName" within the Preferences.Prefs method to 
		something appropriate to your project (or provide an a
		lternative definition of it), and you'll need to call 
		Prefs.Save in your application's Close event. Otherwise, 
		the system is completely automatic.
	#tag EndNote


	#tag Property, Flags = &h21
		Private _Prefs As NamedStrings
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
