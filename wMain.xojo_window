#tag Window
Begin Window wMain
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   215
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "Hemet Police Report Tracker"
   Visible         =   True
   Width           =   560
   Begin PushButton pbEnter
      AutoDeactivate  =   True
      Bold            =   True
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Enter Record"
      Default         =   True
      Enabled         =   True
      Height          =   22
      HelpTag         =   "This button will enter the report with the above options into the spreadsheet.  You can type Return or Enter on the keyboard as a shortcut"
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   269
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   14
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   173
      Underline       =   False
      Visible         =   True
      Width           =   110
   End
   Begin TextField tYear
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   "YR"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   "Report year (defaults to current year)"
      Index           =   -2147483648
      Italic          =   False
      Left            =   50
      LimitText       =   2
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "##"
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   14
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   31
   End
   Begin Label Label2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   11
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      Text            =   "CR#"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   27
   End
   Begin TextField tFile
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   "Enter report number (after dash) or shortcut for status and type(s) of report(s)"
      Index           =   -2147483648
      Italic          =   False
      Left            =   105
      LimitText       =   5
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   14
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   75
   End
   Begin Label Label3
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   89
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      Text            =   "-"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   4
   End
   Begin TextField tOfcr
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   "ID of Reporting Officer"
      Index           =   -2147483648
      Italic          =   False
      Left            =   300
      LimitText       =   5
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "#####"
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   130
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   74
   End
   Begin Label Label4
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      Text            =   "Officer ID"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   132
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   58
   End
   Begin Separator Separator2
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   207
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   386
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Visible         =   True
      Width           =   8
   End
   Begin CheckBox ckInit
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "&Initial"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   40
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      State           =   1
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   48
      Underline       =   False
      Value           =   True
      Visible         =   True
      Width           =   83
   End
   Begin CheckBox ckCustody
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "In &Custody?"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   "This checkbox indicates that the initial report is an in-custody, that must be submitted to court for arraignment.  It is NOT a report document in and of itself, and should always be marked with the appropriate report submittal type (e.g.: Initial)."
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   40
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      State           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   131
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   140
   End
   Begin CheckBox ckSupp
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "&Supplemental"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   40
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      State           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   71
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   93
   End
   Begin CheckBox ckTow
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "&Tow (180)"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   40
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      State           =   0
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   93
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   83
   End
   Begin PopupMenu popSuppCount
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   "Number of supplemental reports approved (1 is default)"
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "1\r\n2\r\n3\r\n4\r\n5\r\n6"
      Italic          =   False
      Left            =   145
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   69
      Underline       =   False
      Visible         =   True
      Width           =   42
   End
   Begin Separator Separator3
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   137
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   195
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   14
      Visible         =   True
      Width           =   8
   End
   Begin RadioButton rbApproved
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "&Approved"
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   223
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   15
      Underline       =   False
      Value           =   True
      Visible         =   True
      Width           =   140
   End
   Begin RadioButton rbReturn
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "&Returned for Corrections"
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   223
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   18
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   36
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   158
   End
   Begin RadioButton rbVoid
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "&Voided"
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   223
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   19
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   57
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   140
   End
   Begin RadioButton rbHoldover
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "&Holdover"
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   223
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   20
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   78
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   84
   End
   Begin CheckBox ckBatchmode
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Batch"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   "When checked, the Tracker is placed into ""Holdover Batch Entry Mode"".  The ""Officer ID"", ""Initial"" report type, and """"Holdover"" status will remain in place.  This allows quick data entry of daily holdover lists for a given officer.\n\nWhen unchecked, Officer ID will be cleared after each entry and default status will revert to ""Approved""."
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   315
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      State           =   0
      TabIndex        =   21
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   78
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   58
   End
   Begin Thread tdUpdateList
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   40
      LockedInPosition=   False
      Priority        =   5
      Scope           =   0
      StackSize       =   0
      TabPanelIndex   =   0
      Top             =   40
      Width           =   32
   End
   Begin Timer TimerGUI
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   80
      LockedInPosition=   False
      Mode            =   2
      Period          =   100
      Scope           =   0
      TabPanelIndex   =   0
      Top             =   80
      Width           =   32
   End
   Begin Timer TimerDBHit
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   100
      LockedInPosition=   False
      Mode            =   2
      Period          =   60000
      Scope           =   0
      TabPanelIndex   =   0
      Top             =   100
      Width           =   32
   End
   Begin BevelButton btnOneMonth
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &cFF000000
      Bevel           =   2
      Bold            =   True
      ButtonType      =   0
      Caption         =   "Over 1 Month - -"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   True
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "This category includes all ""Initial"" reports that have not been ""Approved"" and checked into ""Records"" that are over one month old.  \n\nIf report number is only for a 180, Records will mark the 180 as an initial, clearing this entry."
      Icon            =   0
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   406
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   33
      TabPanelIndex   =   0
      TabStop         =   False
      TextColor       =   &cFFFFFF00
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   172
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   134
   End
   Begin BevelButton btnTwoWeeks
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &cFF7F0000
      Bevel           =   1
      Bold            =   True
      ButtonType      =   0
      Caption         =   "Over 2 Weeks - -"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   True
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "This category includes all ""Initial"" reports that have not been ""Approved"" and checked into ""Records"" that are over two weeks old.  \n\nIf report number is only for a 180, Records will mark the 180 as an initial, clearing this entry."
      Icon            =   0
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   406
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   34
      TabPanelIndex   =   0
      TabStop         =   False
      TextColor       =   &cFFFFFF00
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   141
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   134
   End
   Begin BevelButton btnWaitingCheckin
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c80808000
      Bevel           =   1
      Bold            =   True
      ButtonType      =   0
      Caption         =   "Not Checked - (--) -"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   True
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "This category shows all report groups that a supervisor has ""Approved"" but have not yet been checked into Records.  \n\nThe number in parenthesis shows report packages approved one weekday or less ago and is considered to be in transit to Records, awaiting morning check-in.  These reports will not show up on the detail list.\n\nReport numbers not in parenthesis may depict a LOST report and should be investigated by all supervisors. Holidays are not factored in, so reports will show up in this category on Records holidays.\n"
      Icon            =   0
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   406
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   35
      TabPanelIndex   =   0
      TabStop         =   False
      TextColor       =   &cFFFEFE00
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   110
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   134
   End
   Begin BevelButton btnKickback
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c18BB0500
      Bevel           =   1
      Bold            =   True
      ButtonType      =   0
      Caption         =   "Kicked Back - -"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   True
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "This category shows all report groups in which one or more submitted reports (normally Initial reports) have been ""Returned for Corrections"" either by a supervisor or Records."
      Icon            =   0
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   406
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   36
      TabPanelIndex   =   0
      TabStop         =   False
      TextColor       =   &cFFFFFF00
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   78
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   134
   End
   Begin BevelButton btnHoldover
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c0000FF00
      Bevel           =   1
      Bold            =   True
      ButtonType      =   0
      Caption         =   "Holdover - -"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   True
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "This category shows shows reports in which an officer submitted a holdover request to a supervisor and that supervisor approved the holdover at some point.\n\nPer policy, this shall be done each working day, and for tracking purposes, a supervisor should document that holdover request each day.  \n\nA detail list will show the last day that a supervisor approved the holdover, potentially identifying officers who have not resubmitted the report for subsequent approval to again hold it over."
      Icon            =   0
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   406
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   37
      TabPanelIndex   =   0
      TabStop         =   False
      TextColor       =   &cFFFFFF00
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   46
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   134
   End
   Begin BevelButton btnMissing
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   1
      Bold            =   True
      ButtonType      =   0
      Caption         =   "Unknown - (--) -"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   True
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "This category represents report numbers that have sequentially been pulled, but that no information has been entered into the Tracker.  These reports are added to the Tracker without information when a higher-numbered report is entered.  They may include voids that haven't yet been processed or new reports that haven't yet had holdovers attached.\n\nFor instance, if the highest report number in the Tracker is 3250, and a supervisor approves 3254, report numbers 3251, 3252, and 3253 will be entered as unknown, and will show up under this category.\n\nSince it is normal for reports to be submitted out of sequence, only reports greater than one day old may be of significance.  Same day Unknowns are listed inside the parenthesis."
      Icon            =   0
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   406
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   38
      TabPanelIndex   =   0
      TabStop         =   False
      TextColor       =   &cFFFFFF00
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   14
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   134
   End
   Begin BevelButton BtnFilter
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   4
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Filter: All"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   False
      HasBackColor    =   False
      HasMenu         =   1
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   577
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   39
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "SmallSystem"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   141
      Underline       =   False
      Value           =   False
      Visible         =   False
      Width           =   134
   End
   Begin PopupMenu popTowCount
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   "Number of supplemental reports approved (1 is default)"
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "1\r\n2\r\n3\r\n4\r\n5\r\n6"
      Italic          =   False
      Left            =   145
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   41
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   91
      Underline       =   False
      Visible         =   True
      Width           =   42
   End
   Begin RadioButton rbCheckedIn
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "&Data Entry"
      Enabled         =   False
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   223
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   43
      TabPanelIndex   =   0
      TabStop         =   False
      TextFont        =   "Arial"
      TextSize        =   11.0
      TextUnit        =   0
      Top             =   98
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   84
   End
   Begin PushButton pbUndo
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Undo Last Entry"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   44
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   173
      Underline       =   False
      Visible         =   False
      Width           =   110
   End
   Begin PushButton pbSearch
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Search..."
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   144
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   45
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "Arial"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   173
      Underline       =   False
      Visible         =   False
      Width           =   111
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Close()
		  prefs.SetWindowPos(self.Title,self)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  MemoryDB = new ReportTrackingDB
		  Prefs.GetWindowPos (self.Title, self)
		  
		  // Set default timestamp categories
		  if app.CurrentUser.Supervisor then
		    EntererColumn = "LastWC, WCDateTime"
		    EntererUpdate = "LastWC = "+app.CurrentUser.ID+", WCDateTime = '"
		  else
		    EntererColumn = "LastPSOS, PSOSDateTime"
		    EntererUpdate = "LastPSOS = "+app.CurrentUser.ID+", PSOSDateTime = '"
		  end
		  EntererValue = App.CurrentUser.ID+", '"
		  
		  //Update Report List
		  tdUpdateList.Run
		  
		  //Admin Load from CPROG
		  #if TargetWin32
		    if app.CurrentUser.Administrator and Keyboard.AsyncShiftKey then
		      MsgBox("Searching for CPROG under Admin Load Mode"+EndOfLine+"No blanks will be autofilled.")
		      CPROGDownload False
		    end
		  #endif
		  
		  //Prep for undo function
		  if not app.myDB.Connect then return
		  dim rs As RecordSet
		  rs = app.myDB.FieldSchema("Active")
		  if not app.myDB.Error then
		    UndoCategoryCount = rs.RecordCount
		  end
		  app.myDB.Close
		  
		  // Disable auto CPROG loading if not authorized in environment.
		  // Default GET for not found is unchanged (cprogProcessed Starts false).
		  // CPROG will not load if cprogProcessed true.
		  Prefs.Get("CPROG Autoload", cprogProcessed)
		  cprogProcessed = not cprogProcessed // reverse logic so that a true autoload will make false cprogProcessed
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AfterApproved(rs As RecordSet)
		  // METHODOLOGY:  This means that the Initial Report is approved but not yet checked by records.  If initial is changed other than checked in,
		  //                    we need to confirm it.  Approving supps, etc, just add the number.  Holdover, kickback, ok w/ warning (except kickback by records, just do it)
		  
		  
		  dim sql As String
		  dim d As new date
		  
		  
		  if rbHoldover.Value then // Error.  Supervisor only, and must be initial, so ask if change needed.
		    dim s, status, suppval, towval ,ofcr as string
		    dim i,j as integer
		    if ckSupp.Value then i=popSuppCount.ListIndex+1
		    if ckTow.Value then j=popTowCount.ListIndex+1
		    s=str(i)+" supp(s), and "+str(j) +" 180(s) "
		    if not ConfirmMessage ("This initial report was previously recorded as being approved."+_
		      EndOfLine + EndOfLine + "Do you want to change 1 Initial, "+s+"to holdover status?",  "Holding Over Approved Report" ) then return
		      status = "StatusCode = "+str(ReportTrackingDB.StatusCode.Holdover)+", StatusText = 'Approved to Hold Over', "
		      suppval = "Supp = "+ str(Max(0, rs.Field("Supp").IntegerValue-i))+", "
		      towval = "Tow = "+str(max(0,rs.Field("Tow").IntegerValue-j))+", "
		      ofcr = "OfficerID='"+tOfcr.Text+"', OfficerName='"+app.Officers.Value(tOfcr.Text)+"', "
		      
		      sql = "UPDATE OR ROLLBACK Active Set Initial=0, InitialDone=0, "+status+suppval+towval+ofcr+_
		      entererUpdate + d.SQLDateTime +"' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		      
		      
		      
		    elseif rbVoid.Value then //error possibility, already approved reports
		      if rs.Field("InitialDone").IntegerValue = 1 or rs.Field("SuppDone").IntegerValue>0 or rs.Field("TowDone").IntegerValue>0 then
		        if not app.CurrentUser.Records then
		          MsgBox("ERROR - You are attempting to void a report that has component(s) that have been entered into RMS.  It cannot be voided, except by Records."+_
		          EndOfLine+EndOfLine+"The entry is unchanged.")
		          Return
		        end  //Non-Records employee.  Rejected attempt
		        if ConfirmMessage ("Warning - You are attempting to void a report that already has approved report component(s).  Continuing will erase any"+_
		          "previously approved entries, voiding the entire report."+EndOfLine + EndOfLine + "Are you sure you want to void report number "+_
		          tYear.Text+"-"+tFile.Text+"?",  "Possible Incorrect Status or Report Number") then
		          RptDateSpaceHolder = rs.Field("ReportDate").StringValue
		          OfficerIDSpaceHolder = rs.Field("OfficerID").StringValue
		          OfficerNameSpaceHolder = rs.Field("OfficerName").StringValue
		          sql = "DELETE FROM Active WHERE FullFile= '"+rs.Field("FullFile").StringValue+"';"
		          app.myDB.SQLExecute sql
		          if app.mydb.CheckError (sql) then return
		          InsertRecord
		          return
		        end //Records Employee.  Entered.
		      end  // Report(s) were checked into records
		      //We have only non-checked-in reports
		      if rs.Field("Initial").IntegerValue = 1 or rs.Field("Supp").IntegerValue>0 or rs.Field("Tow").IntegerValue>0 then
		        if not ConfirmMessage ("Warning - You are attempting to void a report that already has approved report component(s).  Continuing will erase any"+_
		          "previously approved entries, voiding the entire report."+EndOfLine + EndOfLine + "Are you sure you want to void report number "+_
		          tYear.Text+"-"+tFile.Text+"?",  "Possible Incorrect Status or Report Number") then return
		        end
		        //Necessary safety checks done.  Do it!
		        sql = "DELETE FROM Active WHERE FullFile= '"+rs.Field("FullFile").StringValue+"';"
		        app.myDB.SQLExecute sql
		        if app.mydb.CheckError (sql) then return
		        InsertRecord
		        return
		        
		        
		        
		      elseif rbApproved.Value then //Can be for adding tow and supp reports, but if an initial report, ask if the counts should be overridden.
		        dim suppval, towval as string
		        dim i,j As Integer
		        if not ckSupp.Value and not ckTow.Value then
		          MsgBox ("This initial report was previously marked as approved.  The entry is unchanged.")
		          return
		        end
		        if ckSupp.Value then i=popSuppCount.ListIndex+1
		        if ckTow.Value then j=popTowCount.ListIndex+1
		        if ckInit.Value then
		          if not ConfirmMessage("You are attempting to approve a previously approved initial, along with "+str(i)+" supplemental report(s) and "+_
		            str(j)+" 180(s).  This will reset the total number of approved reports to these values." +EndOfLine +EndOfLine+ "Do you want to reset the approved "+_
		            "report numbers?", "Approving a Previously Approved Report") then return
		            suppval = "Supp="+str(i)+", "
		            towval = "Tow="+str(j)+", "
		          else
		            suppval = "Supp="+str(rs.Field("Supp").IntegerValue+i)+", "
		            towval = "Tow="+str(rs.Field("Tow").IntegerValue+j)+", "
		          end
		          
		          sql = "UPDATE OR ROLLBACK Active Set "+suppval+towval+entererUpdate+d.SQLDateTime+_
		          "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		          
		          
		          
		        elseif rbCheckedIn.Value then
		          //Normal Response.  reduce approved by one, and add 'done' by one.  Records Only.
		          dim status, initval, suppval, towval as string
		          
		          status = "StatusCode = "+str(ReportTrackingDB.StatusCode.CheckedIntoRecords)+", StatusText = 'Checked into Records', "
		          initval = "Initial=0, InitialDone = 1, "
		          if ckSupp.Value then
		            dim i As integer
		            i=max(0,rs.Field("Supp").IntegerValue-(popSuppCount.ListIndex+1))
		            suppval = "Supp = "+str(i)+", SuppDone = "+str(popSuppCount.ListIndex+1+rs.Field("SuppDone").IntegerValue)+", "
		          end
		          if ckTow.Value then
		            dim i as integer
		            i=max(0,rs.Field("Tow").IntegerValue-(popTowCount.ListIndex+1))
		            towval = "Tow = "+str(i)+", TowDone = "+str(popSuppCount.ListIndex+1+rs.Field("TowDone").IntegerValue)+", "
		          end
		          
		          sql = "UPDATE OR ROLLBACK Active Set "+status+initval+suppval+towval+entererUpdate+d.SQLDateTime+_
		          "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		          
		          
		        ElseIf rbReturn.Value then
		          //approved returned by either, but confirm inits with WC.  auto on unapproved or non-initial.  reduce values by one
		          dim status, initval, suppval, towval, employee as string
		          if ckInit.Value then // error check
		            If not app.CurrentUser.Records then //confirm
		              if not ConfirmMessage ("You are attempting to return an initial report that you already approved.  This is allowed, but may be"+_
		                " an error.  Please check the report number and returned components before continuing."+EndOfLine+EndOfLine+_
		                "Do you wish to continue?","Possible Error.  Returning Approved Initial.") then return
		              end
		              status = "StatusCode = "+str(ReportTrackingDB.StatusCode.ReturnedForCorrections)+", StatusText = 'Returned for Corrections', "
		              initval = "Initial=0, InitialDone = 0, "
		            end //ckInit.Value
		            if app.CurrentUser.Records and ((ckSupp.Value and rs.Field("SuppDone").IntegerValue>0) or (ckTow.Value and rs.Field("TowDone").IntegerValue>0)) then
		              // we don't have a way to tell if these have been checked in, so we ask.
		              if ConfirmMessage ("Have these Supplemental and/or Tow report(s) been checked into Records already?", "Data Entry Question") then
		                if ckSupp.Value then suppval = "SuppDone = "+ str(Max(0, rs.Field("SuppDone").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		                if ckTow.Value then towval = "TowDone = "+ str(Max(0, rs.Field("TowDone").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		              end
		            end
		            if (ckSupp.Value and rs.Field("Supp").IntegerValue>0) or (ckTow.Value and rs.Field("Tow").IntegerValue>0) then
		              // we don't have a way to tell if these have been checked in, so we ask.
		              if ConfirmMessage ("Have these Supplemental and/or Tow report(s) been approved by a supervisor already?", "Data Entry Question") then
		                if ckSupp.Value then suppval = suppval + "Supp = "+ str(Max(0, rs.Field("Supp").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		                if ckTow.Value then towval = towval + "Tow = "+ str(Max(0, rs.Field("Tow").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		              end
		            end
		            employee = "OfficerID = "+tOfcr.Text+", OfficerName = '"+app.Officers.Value(tOfcr.Text)+"', "
		            
		            sql = "UPDATE OR ROLLBACK Active Set "+status+initval+suppval+towval+employee+entererUpdate+d.SQLDateTime+_
		            "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		            
		          end // Main Selector
		          
		          
		          app.mydb.SQLExecute sql
		          call app.mydb.CheckError (sql)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AfterCheckedIn(rs As RecordSet)
		  //METHODOLOGY - Records entered the initial.  If supervisor, error for anything but approved supps / tows
		  //     For Records, just confirm kickbacks of initials, do kickbacks of others
		  dim sql As string
		  dim d As new date
		  
		  if app.CurrentUser.Records then
		    
		    if rbVoid.Value then //error possibility, already approved reports
		      if not ConfirmMessage ("Warning - You are attempting to void a report that already has Checked-in report component(s).  Continuing will erase any"+_
		        "previously approved entries, voiding the entire report."+EndOfLine + EndOfLine + "Are you sure you want to void report number "+_
		        tYear.Text+"-"+tFile.Text+"?",  "Possible Incorrect Status or Report Number") then return
		        RptDateSpaceHolder = rs.Field("ReportDate").StringValue
		        OfficerIDSpaceHolder = rs.Field("OfficerID").StringValue
		        OfficerNameSpaceHolder = rs.Field("OfficerName").StringValue
		        sql = "DELETE FROM Active WHERE FullFile= '"+rs.Field("FullFile").StringValue+"';"
		        app.myDB.SQLExecute sql
		        if app.mydb.CheckError (sql) then return
		        InsertRecord
		        return
		        
		        
		      elseif rbCheckedIn.Value then
		        dim suppval, towval as string
		        if ckSupp.Value then
		          dim i As integer
		          i=max(0,rs.Field("Supp").IntegerValue-(popSuppCount.ListIndex+1))
		          suppval = "Supp = "+str(i)+", SuppDone = "+str(popSuppCount.ListIndex+1+rs.Field("SuppDone").IntegerValue)+", "
		        end
		        if ckTow.Value then
		          dim i as integer
		          i=max(0,rs.Field("Tow").IntegerValue-(popTowCount.ListIndex+1))
		          towval = "Tow = "+str(i)+", TowDone = "+str(popSuppCount.ListIndex+1+rs.Field("TowDone").IntegerValue)+", "
		        end
		        
		        sql = "UPDATE OR ROLLBACK Active Set "+suppval+towval+entererUpdate+d.SQLDateTime+_
		        "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		        
		        
		      ElseIf rbReturn.Value then
		        //approved returned by either, but confirm inits with WC.  auto on unapproved or non-initial.  reduce values by one
		        dim status, initval, suppval, towval, employee as string
		        if ckInit.Value then // error check
		          if not ConfirmMessage ("You are attempting to return an initial report that you already checked in.  This is allowed, but may be"+_
		            " an error.  Please check the report number and returned components before continuing."+EndOfLine+EndOfLine+_
		            "Do you wish to continue?","Possible Error.  Returning Checked-in Initial.") then return
		            status = "StatusCode = "+str(ReportTrackingDB.StatusCode.ReturnedForCorrections)+", StatusText = 'Returned for Corrections', "
		            initval = "Initial=0, InitialDone = 0, "
		          end //ckInit.Value
		          if ((ckSupp.Value and rs.Field("SuppDone").IntegerValue>0) or (ckTow.Value and rs.Field("TowDone").IntegerValue>0)) then
		            // we don't have a way to tell if these have been checked in, so we ask.
		            if ConfirmMessage ("Have these Supplemental and/or Tow report(s) been checked into Records already?", "Data Entry Question") then
		              if ckSupp.Value then suppval = "SuppDone = "+ str(Max(0, rs.Field("SuppDone").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		              if ckTow.Value then towval = "TowDone = "+ str(Max(0, rs.Field("TowDone").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		            end
		          end
		          if ckSupp.Value then suppval = suppval + "Supp = "+ str(Max(0, rs.Field("Supp").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		          if ckTow.Value then towval = towval + "Tow = "+ str(Max(0, rs.Field("Tow").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		          employee = "OfficerID = "+tOfcr.Text+", OfficerName = '"+app.Officers.Value(tOfcr.Text)+"', "
		          
		          sql = "UPDATE OR ROLLBACK Active Set "+status+initval+suppval+towval+employee+entererUpdate+d.SQLDateTime+_
		          "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		        end // Main Selector
		      end//Records
		      
		      If app.CurrentUser.Supervisor then
		        if rbApproved.Value and not ckInit.Value then //Only valid response for supervisors
		          dim i, j As integer
		          dim suppval,towval as string
		          
		          if ckSupp.Value then i=popSuppCount.ListIndex+1
		          if ckTow.Value then j=popTowCount.ListIndex+1
		          suppval = "Supp="+str(rs.Field("Supp").IntegerValue+i)+", "
		          towval = "Tow="+str(rs.Field("Tow").IntegerValue+j)+", "
		          sql = "UPDATE OR ROLLBACK Active Set "+suppval+towval+entererUpdate+d.SQLDateTime+_
		          "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		        else
		          MsgBox ("Warning - You are attempting alter an initial report that has had data entry done by Records.  "+_
		          EndOfLine+"Records must send the initial report back after confirming that any RMS changes are made."+EndOfLine+EndOfLine+_
		          "This entry was not changed.")
		          return
		        end
		      end // Records vs Supervisor
		      
		      
		      
		      
		      
		      app.mydb.SQLExecute sql
		      call app.mydb.CheckError (sql)
		      
		      
		      
		      
		      
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AfterHoldover(rs As RecordSet)
		  // METHODOLOGY :  Initial was previously held over.  It can automatically be moved to any other position.  Confirm Officer Info
		  //                             if it changes and then add new timestamp
		  
		  
		  dim sql As String
		  dim d As new date
		  
		  
		  if rbHoldover.Value then // still held over, error possibility is wrong officer.   Supervisor only
		    if  rs.Field("OfficerID").StringValue <> tOfcr.Text then
		      if not ConfirmMessage ("This report was previously held over for "+rs.Field("OfficerName").StringValue+" ("+rs.Field("OfficerID").StringValue+_
		        ").  "+ EndOfLine + EndOfLine + "Do you want to change it to "+ app.Officers.Value(tOfcr.Text) +_
		        " ("+tOfcr.Text+")?",  "Possible Incorrect Officer Information" ) then return
		      end
		      
		      sql = "UPDATE OR ROLLBACK Active Set OfficerID='"+tOfcr.Text+"', OfficerName='"+app.Officers.Value(tOfcr.Text)+"', LastWC=" +_
		      app.CurrentUser.ID+", WCDateTime='"+d.SQLDateTime+"' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		      
		      
		      
		      
		      
		      
		    elseif rbVoid.Value then //error possibility, already approved reports
		      if rs.Field("InitialDone").IntegerValue = 1 or rs.Field("SuppDone").IntegerValue>0 or rs.Field("TowDone").IntegerValue>0 then
		        if not app.CurrentUser.Records then
		          MsgBox("ERROR - You are attempting to void a report that has component(s) that have been entered into RMS.  It cannot be voided, except by Records."+_
		          EndOfLine+EndOfLine+"The entry is unchanged.")
		          Return
		        end  //Non-Records employee.  Rejected attempt
		        if ConfirmMessage ("Warning - You are attempting to void a report that already has approved report component(s).  Continuing will erase any"+_
		          "previously approved entries, voiding the entire report."+EndOfLine + EndOfLine + "Are you sure you want to void report number "+_
		          tYear.Text+"-"+tFile.Text+"?",  "Possible Incorrect Status or Report Number") then
		          RptDateSpaceHolder = rs.Field("ReportDate").StringValue
		          OfficerIDSpaceHolder = rs.Field("OfficerID").StringValue
		          OfficerNameSpaceHolder = rs.Field("OfficerName").StringValue
		          sql = "DELETE FROM Active WHERE FullFile= '"+rs.Field("FullFile").StringValue+"';"
		          app.myDB.SQLExecute sql
		          if app.mydb.CheckError (sql) then return
		          InsertRecord
		          return
		        end //Records Employee.  Entered.
		      end  // Report(s) were checked into records
		      
		      //We have only non-checked-in reports
		      if rs.Field("Initial").IntegerValue = 1 or rs.Field("Supp").IntegerValue>0 or rs.Field("Tow").IntegerValue>0 then
		        if not ConfirmMessage ("Warning - You are attempting to void a report that already has approved report component(s).  Continuing will erase any"+_
		          "previously approved entries, voiding the entire report."+EndOfLine + EndOfLine + "Are you sure you want to void report number "+_
		          tYear.Text+"-"+tFile.Text+"?",  "Possible Incorrect Status or Report Number") then return
		        end
		        //Necessary safety checks done.  Do it!
		        sql = "DELETE FROM Active WHERE FullFile= '"+rs.Field("FullFile").StringValue+"';"
		        app.myDB.SQLExecute sql
		        if app.mydb.CheckError (sql) then return
		        InsertRecord
		        return
		        
		        
		        
		        
		        
		        
		        
		        
		      elseif rbApproved.Value then //Normal progression.  Supervisor only.  Add supps and tows.  error mode of init and initdone
		        
		        dim status As String//status is nothing by default.  only change status with initial.
		        dim initval, suppval, towval, custodyval as string
		        if ckInit.Value then
		          status = "StatusCode = "+str(ReportTrackingDB.StatusCode.ApprovedByWC)+", StatusText = 'Approved to Records', "
		          if rs.Field("InitialDone").IntegerValue <1 then  //only add initial if one is not checked in.  False is an error by w/c
		            initval = "Initial = 1, "
		          end
		          if ckCustody.Value then
		            custodyval = "InCustody ='True', "
		          else
		            custodyval = "InCustody = 'False', "
		          end
		        end //ckInit.Value
		        if ckSupp.Value then suppval = "Supp = "+ str(popSuppCount.ListIndex+1+rs.Field("Supp").IntegerValue)+", "
		        if ckTow.Value then towval = "Tow = "+str(popTowCount.ListIndex+1+rs.Field("Tow").IntegerValue)+", "
		        // add tow and supp popup values to the existing fields.
		        
		        sql = "UPDATE OR ROLLBACK Active Set "+status+initval+suppval+towval+custodyval+entererUpdate+d.SQLDateTime+_
		        "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		        
		        
		        
		        
		        
		      elseif rbCheckedIn.Value then
		        //abnormal but allowed for inits.  expected for supps/tows.  reduce approved by one, and add 'done' by one.  Records Only.
		        
		        dim status, initval, suppval, towval as string
		        if rs.Field("Initial").IntegerValue = 0 and rs.Field("InitialDone").IntegerValue = 0 and ckInit.Value then
		          dim s As string
		          if ckTow.Value then
		            s = EndOfLine+"If this is a ""CHP180-only"" report, this will correctly change the tow to an initial report."
		          end
		          if not ConfirmMessage ("You are checking in an initial report that hasn't been entered as approved by a supervisor."+_
		            "Please confirm the file number is correct and the report is an initial."+s+EndOfLine+EndOfLine+_
		            "Do you want to continue?","Checking-in Unapproved Initial") then return
		          end
		          if ckInit.Value then
		            status = "StatusCode = "+str(ReportTrackingDB.StatusCode.CheckedIntoRecords)+", StatusText = 'Checked into Records', "
		            initval = "Initial=0, InitialDone = 1, "
		          end
		          if ckSupp.Value then
		            dim i As integer
		            i=max(0,rs.Field("Supp").IntegerValue-(popSuppCount.ListIndex+1))
		            suppval = "Supp = "+str(i)+", SuppDone = "+str(popSuppCount.ListIndex+1+rs.Field("SuppDone").IntegerValue)+", "
		          end
		          if ckTow.Value then
		            dim i as integer
		            i=max(0,rs.Field("Tow").IntegerValue-(popTowCount.ListIndex+1))
		            towval = "Tow = "+str(i)+", TowDone = "+str(popSuppCount.ListIndex+1+rs.Field("TowDone").IntegerValue)+", "
		          end
		          
		          sql = "UPDATE OR ROLLBACK Active Set "+status+initval+suppval+towval+entererUpdate+d.SQLDateTime+_
		          "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		          
		          
		          
		          
		        ElseIf rbReturn.Value then
		          //3 options for inits, the returned report has been checked in, it's been approved, or neither.
		          //checked in can only be returned by records ,after confirmation.
		          //approved returned by either, but confirm inits with WC.  auto on unapproved or non-initial.  reduce values by one
		          
		          dim status, initval, suppval, towval, employee as string
		          
		          if ckInit.Value then // error check
		            if rs.Field("InitialDone").IntegerValue=1 and not app.CurrentUser.Records then  // option 1
		              MsgBox ("Warning - You are attempting to send an initial report back for corrections that has had data entry done by Records.  "+_
		              EndOfLine+"Records must send the initial report back after confirming that any RMS changes are made."+EndOfLine+EndOfLine+_
		              "This entry was not changed.")
		              return
		            end
		            If rs.Field("Initial").IntegerValue=1 and not app.CurrentUser.Records then //option 2
		              if not ConfirmMessage ("You are attempting to return an initial report that you already approved.  This is allowed, but may be"+_
		                " an error.  Please check the report number and returned components before continuing."+EndOfLine+EndOfLine+_
		                "Do you wish to continue?","Possible Error.  Returning Approved Initial.") then return
		              end
		              status = "StatusCode = "+str(ReportTrackingDB.StatusCode.ReturnedForCorrections)+", StatusText = 'Returned for Corrections', "
		              initval = "Initial=0, InitialDone = 0, "
		            end //ckInit.Value
		            
		            if app.CurrentUser.Records and ((ckSupp.Value and rs.Field("SuppDone").IntegerValue>0) or (ckTow.Value and rs.Field("TowDone").IntegerValue>0)) then
		              // we don't have a way to tell if these have been checked in, so we ask.
		              if ConfirmMessage ("Have these Supplemental and/or Tow report(s) been checked into Records already?", "Data Entry Question") then
		                if ckSupp.Value then suppval = "SuppDone = "+ str(Max(0, rs.Field("SuppDone").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		                if ckTow.Value then towval = "TowDone = "+ str(Max(0, rs.Field("TowDone").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		              end
		            end
		            if (ckSupp.Value and rs.Field("Supp").IntegerValue>0) or (ckTow.Value and rs.Field("Tow").IntegerValue>0) then
		              // we don't have a way to tell if these have been checked in, so we ask.
		              if ConfirmMessage ("Have these Supplemental and/or Tow report(s) been approved by a supervisor already?", "Data Entry Question") then
		                if ckSupp.Value then suppval = suppval + "Supp = "+ str(Max(0, rs.Field("Supp").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		                if ckTow.Value then towval = towval + "Tow = "+ str(Max(0, rs.Field("Tow").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		              end
		            end
		            employee = "OfficerID = "+tOfcr.Text+", OfficerName = '"+app.Officers.Value(tOfcr.Text)+"', "
		            
		            sql = "UPDATE OR ROLLBACK Active Set "+status+initval+suppval+towval+employee+entererUpdate+d.SQLDateTime+_
		            "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		            
		          end // Main Selector
		          
		          
		          app.mydb.SQLExecute sql
		          call app.mydb.CheckError (sql)
		          
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AfterKickback(rs As RecordSet)
		  // METHODOLOGY:  This means that the Initial Report was previously kicked back (ID No is entered).
		  //                   (most likely holdover or approved so just do it),  Confirm data entry and void.  for kickback, just update info
		  //                    for supps, process the same but don't change status
		  
		  
		  dim sql As String
		  dim d As new date
		  
		  
		  if rbHoldover.Value then // Error mode:.  id number changing. Confirm that.
		    dim status, ofcr as string
		    
		    if rs.Field("OfficerID").StringValue <> tOfcr.Text then
		      if not ConfirmMessage ("This initial report was previously kicked back to "+ rs.Field("OfficerName").StringValue +_
		        " ("+rs.Field("OfficerID").StringValue+").  "+ EndOfLine + EndOfLine +_
		        "Do you want to change the officer to "+app.Officers.Value(tOfcr.Text)+"?",  "Holding Over Approved Report" ) then return
		      end
		      status = "StatusCode = "+str(ReportTrackingDB.StatusCode.Holdover)+", StatusText = 'Approved to Hold Over', "
		      ofcr = "OfficerID='"+tOfcr.Text+"', OfficerName='"+app.Officers.Value(tOfcr.Text)+"', "
		      
		      sql = "UPDATE OR ROLLBACK Active Set Initial=0, InitialDone=0, "+status+ofcr+_
		      entererUpdate + d.SQLDateTime +"' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		      
		      
		      
		    elseif rbVoid.Value then //error possibility, already approved reports
		      if rs.Field("InitialDone").IntegerValue = 1 or rs.Field("SuppDone").IntegerValue>0 or rs.Field("TowDone").IntegerValue>0 then
		        if not app.CurrentUser.Records then
		          MsgBox("ERROR - You are attempting to void a report that has component(s) that have been entered into RMS.  It cannot be voided, except by Records."+_
		          EndOfLine+EndOfLine+"The entry is unchanged.")
		          Return
		        end  //Non-Records employee.  Rejected attempt
		        if ConfirmMessage ("Warning - You are attempting to void a report that already has approved report component(s).  Continuing will erase any"+_
		          "previously approved entries, voiding the entire report."+EndOfLine + EndOfLine + "Are you sure you want to void report number "+_
		          tYear.Text+"-"+tFile.Text+"?",  "Possible Incorrect Status or Report Number") then
		          RptDateSpaceHolder = rs.Field("ReportDate").StringValue
		          OfficerIDSpaceHolder = rs.Field("OfficerID").StringValue
		          OfficerNameSpaceHolder = rs.Field("OfficerName").StringValue
		          sql = "DELETE FROM Active WHERE FullFile= '"+rs.Field("FullFile").StringValue+"';"
		          app.myDB.SQLExecute sql
		          if app.mydb.CheckError (sql) then return
		          InsertRecord
		          return
		        end //Records Employee.  Entered.
		      end  // Report(s) were checked into records
		      //We have only non-checked-in reports
		      if rs.Field("Initial").IntegerValue = 1 or rs.Field("Supp").IntegerValue>0 or rs.Field("Tow").IntegerValue>0 then
		        if not ConfirmMessage ("Warning - You are attempting to void a report that already has approved report component(s).  Continuing will erase any"+_
		          "previously approved entries, voiding the entire report."+EndOfLine + EndOfLine + "Are you sure you want to void report number "+_
		          tYear.Text+"-"+tFile.Text+"?",  "Possible Incorrect Status or Report Number") then return
		        end
		        //Necessary safety checks done.  Do it!
		        sql = "DELETE FROM Active WHERE FullFile= '"+rs.Field("FullFile").StringValue+"';"
		        app.myDB.SQLExecute sql
		        if app.mydb.CheckError (sql) then return
		        InsertRecord
		        return
		        
		        
		        
		      elseif rbApproved.Value then //default progression.  Just do it.
		        dim status, init, suppval, towval as string
		        dim i,j As Integer
		        if ckSupp.Value then i=popSuppCount.ListIndex+1+rs.Field("Supp").IntegerValue
		        if ckTow.Value then j=popTowCount.ListIndex+1+rs.Field("Tow").IntegerValue
		        if ckInit.Value then
		          init = "Initial = 1, "
		          status = "StatusCode = "+str(ReportTrackingDB.StatusCode.ApprovedByWC)+", StatusText = 'Approved to Records', "
		        end
		        suppval = "Supp="+str(i)+", "
		        towval = "Tow="+str(j)+", "
		        
		        sql = "UPDATE OR ROLLBACK Active Set "+status+init+suppval+towval+entererUpdate+d.SQLDateTime+_
		        "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		        
		        
		        
		      elseif rbCheckedIn.Value then
		        //Normal Response for approved supps/tows.  reduce approved by one, and add 'done' by one.  Records Only.  For initials, confirm.
		        //abnormal but allowed for inits.  expected for supps/tows.
		        
		        dim status, initval, suppval, towval as string
		        if rs.Field("Initial").IntegerValue = 0 and rs.Field("InitialDone").IntegerValue = 0 and ckInit.Value then
		          dim s As string
		          if ckTow.Value then
		            s = EndOfLine+"If this is a ""CHP180-only"" report, this will correctly change the tow to an initial report."
		          end
		          if not ConfirmMessage ("You are checking in an initial report that hasn't been entered as approved by a supervisor."+_
		            "Please confirm the file number is correct and the report is an initial."+s+EndOfLine+EndOfLine+_
		            "Do you want to continue?","Checking-in Unapproved Initial") then return
		          end
		          if ckInit.Value then
		            status = "StatusCode = "+str(ReportTrackingDB.StatusCode.CheckedIntoRecords)+", StatusText = 'Checked into Records', "
		            initval = "Initial=0, InitialDone = 1, "
		          end
		          if ckSupp.Value then
		            dim i As integer
		            i=max(0,rs.Field("Supp").IntegerValue-(popSuppCount.ListIndex+1))
		            suppval = "Supp = "+str(i)+", SuppDone = "+str(popSuppCount.ListIndex+1+rs.Field("SuppDone").IntegerValue)+", "
		          end
		          if ckTow.Value then
		            dim i as integer
		            i=max(0,rs.Field("Tow").IntegerValue-(popTowCount.ListIndex+1))
		            towval = "Tow = "+str(i)+", TowDone = "+str(popSuppCount.ListIndex+1+rs.Field("TowDone").IntegerValue)+", "
		          end
		          
		          sql = "UPDATE OR ROLLBACK Active Set "+status+initval+suppval+towval+entererUpdate+d.SQLDateTime+_
		          "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		          
		          
		          
		        ElseIf rbReturn.Value then
		          //Reduce values by one on approved non-initials.  Otherwise, just update date/time/officer
		          dim suppval, towval, employee as string
		          
		          if app.CurrentUser.Records and ((ckSupp.Value and rs.Field("SuppDone").IntegerValue>0) or (ckTow.Value and rs.Field("TowDone").IntegerValue>0)) then
		            // we don't have a way to tell if these have been checked in, so we ask.
		            if ConfirmMessage ("Have these Supplemental and/or Tow report(s) been checked into Records already?", "Data Entry Question") then
		              if ckSupp.Value then suppval = "SuppDone = "+ str(Max(0, rs.Field("SuppDone").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		              if ckTow.Value then towval = "TowDone = "+ str(Max(0, rs.Field("TowDone").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		            end
		          end
		          if (ckSupp.Value and rs.Field("Supp").IntegerValue>0) or (ckTow.Value and rs.Field("Tow").IntegerValue>0) then
		            // we don't have a way to tell if these have been checked in, so we ask.
		            if ConfirmMessage ("Have these Supplemental and/or Tow report(s) been approved by a supervisor already?", "Data Entry Question") then
		              if ckSupp.Value then suppval = suppval + "Supp = "+ str(Max(0, rs.Field("Supp").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		              if ckTow.Value then towval = towval + "Tow = "+ str(Max(0, rs.Field("Tow").IntegerValue-(popSuppCount.ListIndex+1))) +", "
		            end
		          end
		          employee = "OfficerID = "+tOfcr.Text+", OfficerName = '"+app.Officers.Value(tOfcr.Text)+"', "
		          
		          sql = "UPDATE OR ROLLBACK Active Set "+suppval+towval+employee+entererUpdate+d.SQLDateTime+_
		          "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		          
		        end // Main Selector
		        
		        
		        app.mydb.SQLExecute sql
		        call app.mydb.CheckError (sql)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AfterVoid(rs As RecordSet)
		  // METHODOLOGY :  Initial was previously voided.  It can be unvoided with a confirmation, and if so, it is blanked out like Missing.
		  //                              No matter what category we're changing to, we're going overwrite everything but date and
		  //                              the oficer name.   RptDateSpaceHolder and OfficerSpaceHolder used.  Status will always be changed.
		  
		  
		  // Warning Notice, and if asserted, overwrite all, preserve date if known
		  if not rbVoid.Value then
		    dim i as Integer
		    i=MsgBox("Filenumber "+rs.Field("FullFile").StringValue+" was previously reported as ""Voided""."+_
		    "While the status may be changed, this may be an incorrect report number.  Please double check the file number."+_
		    EndOfLine+EndOfLine+"Are you sure you want to change the designation from Void?" , 4+512, "Possible Incorrect Report Number")
		    if i=7 then //User pushed "No" (default)
		      Return
		    end
		  end
		  RptDateSpaceHolder  = rs.Field("ReportDate").StringValue
		  OfficerIDSpaceHolder = rs.Field("OfficerID").StringValue
		  OfficerNameSpaceHolder = rs.Field("OfficerName").StringValue
		  dim sql as string = "DELETE FROM Active WHERE FullFile= '"+rs.Field("FullFile").StringValue+"';"
		  app.MyDB.SQLExecute sql
		  call app.MyDB.CheckError sql
		  InsertRecord
		  Return
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AutomatedFill(File as string, theDate as string, Ofcr as string, IncludeBlanks as Boolean) As Boolean
		  dim db As ReportTrackingDB = app.myDB
		  dim rs As RecordSet
		  dim i, j,k As integer
		  dim sql As string = "SELECT FullFile, ReportDate, StatusCode, OfficerID FROM Active WHERE FullFile = '"+File+"';"
		  dim year As string = left(File,2)
		  dim frag as string = mid (File, 4)
		  rs=db.SQLSelect(sql)
		  if db.Error and app.CurrentUser.Administrator then call db.CheckError (sql)
		  if db.error then return False
		  if rs= nil or rs.RecordCount=0 then // Record not found.  Do New.
		    if IncludeBlanks then //  Insert Blanks between new entry and max old entry
		      sql = "SELECT Max(FileNo) FROM Active WHERE FileYear="+year+";"
		      if db.Error and app.CurrentUser.Administrator then call db.CheckError (sql)
		      rs=db.SQLSelect (sql)
		      if  db.error then return False
		      j = rs.IdxField(1).IntegerValue + 1
		      k = val(frag)-1
		      db.SQLExecute "BEGIN TRANSACTION;"
		      if db.Error then return False
		      for i = j to k
		        sql = "INSERT OR ROLLBACK INTO Active (FileYear, FileNo, FullFile, ReportDate, StatusCode, StatusText) VALUES ("+_
		        year+", "+str(i)+", '"+year+"-"+str(i)+"', '"+ theDate+ "', "+str(ReportTrackingDB.StatusCode.Unknown) +", 'CPROG Autofill');"
		        db.SQLExecute (sql)
		        if db.Error and app.CurrentUser.Administrator then call db.CheckError (sql)
		        if db.Error then return false
		      next //blank entries
		      db.SQLExecute "END TRANSACTION"
		    end //IncludeBlanks
		    //Officer
		    if ofcr<>"" and app.Officers.HasKey (Ofcr) then
		      ofcr = " '"+Ofcr+"', '"+app.Officers.Value(Ofcr)+"');"
		    else
		      ofcr = "NULL,NULL );"
		    end
		    sql = "INSERT OR ROLLBACK INTO Active (FileYear, FileNo, FullFile, ReportDate, StatusCode, StatusText, OfficerID, OfficerName) VALUES ("+_
		    year+", "+Frag+", '"+File+"', '"+ theDate+"', "+  str(ReportTrackingDB.StatusCode.Unknown) +", 'CPROG Entry', "+ofcr
		    db.SQLExecute (sql)
		    if db.Error and app.CurrentUser.Administrator then call db.CheckError (sql)
		    return not db.Error
		    
		  else //File exists
		    if rs.Field("OfficerID").StringValue="" and app.Officers.HasKey (Ofcr) then
		      ofcr = "OfficerID= '"+ofcr+"', OfficerName= '"+app.Officers.Value(ofcr)+"', "
		    else
		      ofcr = ""
		    end
		    sql = "UPDATE OR ROLLBACK Active Set "+ Ofcr+ "ReportDate = '"+theDate+"' WHERE FullFile='" +File+"';"
		    db.SQLExecute (sql)
		    if db.Error and app.CurrentUser.Administrator then call db.CheckError (sql)
		    return not db.Error
		  end
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ConfirmMessage(msg as String, title As String) As Boolean
		  dim i As integer
		  i=MsgBox(msg, 4+512,title)
		  if i=7 then //User pushed "No" (default)
		    MsgBox ("The report entry was not changed.")
		    Return False
		  else
		    return True
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CPROGDownload(FillInBlanks As Boolean = True)
		  dim excel as new ExcelApplication
		  dim book as ExcelWorkbook
		  dim wSheet as ExcelWorksheet
		  dim rng As ExcelRange
		  dim found as Boolean = false
		  Dim s As String
		  Dim cr, dt, ofcr, lastrow As Integer
		  dim i As integer = excel.Workbooks.Count
		  
		  Prefs.Get("CPROG WindowName",s)
		  for j as integer =1 to i
		    book = excel.Workbooks(j)
		    if left(book.Name,s.len)=s then
		      found=True
		      exit
		    end
		  next
		  if not found then return
		  
		  //book represents CPROG
		  wSheet = book.ActiveSheet
		  wSheet.Activate
		  
		  dim f1,f2,f3 as string
		  prefs.Get("CPROG FieldNumber", f1)
		  prefs.Get("CPROG FieldDate", f2)
		  prefs.Get("CPROG FieldOfficer", f3)
		  For i = 1 To 10
		    rng = wSheet.Cells(1,i)
		    if rng.Value<>nil then
		      s=rng.Value
		      Select Case s
		      Case f1
		        cr = i
		      Case f2
		        dt = i
		      Case f3
		        ofcr = i
		      End Select
		    end
		  Next
		  If cr = 0 Or dt = 0 Or ofcr = 0 Then Return
		  lastrow = Wsheet.Cells.Find("CR", wSheet.Cells(1,cr), Office.xlValues, Office.xlPart, Office.xlByRows, Office.xlPrevious, False, False).Row
		  if lastrow<2 then return
		  
		  // We now have CPROG Data.  Process ////
		  dim rptNo, OfcrID, theDate as string
		  dim d As Date
		  if not app.myDB.Connect then return
		  
		  For i = 2 To  lastrow
		    rng = wSheet.Cells(i,cr)
		    if rng.Value<> nil then
		      s=rng.Value
		      rptNo = mid(s,4)
		    end
		    rng = wSheet.Cells(i,dt)
		    if rng.Value<> nil then
		      s=rng.Text
		      if ParseDate (s, d) then theDate = d.SQLDate
		    end
		    rng = WSheet.Cells(i, ofcr)
		    if rng.Value <> nil then
		      dim first,last as integer
		      s=rng.Value
		      first = s.InStr("(")
		      last = s.InStr(")")
		      OfcrID = mid(s,first+1,last-first-1)
		      if len(OfcrID) = 4 then break
		    end
		    // DBASE STUFF HERE///
		    if not AutomatedFill(rptNo, theDate, OfcrID, FillInBlanks) then Return  //AutomatedFill will add/change files and rerturn success flag
		    rptNo=""
		    theDate=""
		    OfcrID=""  //reset fields for next loop
		  Next
		  
		  app.myDB.Close
		  cprogProcessed = True
		  
		Exception err as OLEException
		  MsgBox (err.message)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub InsertRecord()
		  dim rs As RecordSet
		  dim i,j,k As integer
		  dim db As ReportTrackingDB = App.myDB
		  dim sql as String
		  dim d As new date
		  dim stat, ofcr, inCustody, cnts as string
		  
		  
		  ////////// Format SQL fragments based upon various radio button options
		  
		  //Date.  If this is an update to a missing report (via Update Record), RptDateSpaceHolder will not be "", so preserve it, and officer info from CPROG.
		  if RptDateSpaceHolder ="" then
		    RptDateSpaceHolder =d.SQLDate
		  end
		  
		  //Status
		  stat = str(ReportTrackingDB.StatusCode.Unknown)+", 'Unknown', "
		  If rbHoldover.Value and ckInit.Value then
		    stat = str(ReportTrackingDB.StatusCode.Holdover)+", 'Approved to Hold Over', "
		  ElseIf rbApproved.Value and ckInit.Value then
		    stat =str(ReportTrackingDB.StatusCode.ApprovedByWC)+", 'Approved to Records', "
		  Elseif rbReturn.Value and ckInit.Value then
		    stat =str(ReportTrackingDB.StatusCode.ReturnedForCorrections)+", 'Returned for Corrections', "
		  ElseIf rbCheckedIn.Value and ckInit.Value then
		    stat =str(ReportTrackingDB.StatusCode.CheckedIntoRecords)+", 'Checked into Records', "
		  ElseIf rbVoid.Value then
		    stat = str(ReportTrackingDB.StatusCode.Voided) + ", 'Voided Report Number', "
		  end
		  
		  //Officer
		  if app.Officers.HasKey (tOfcr.Text) then
		    ofcr = "'"+tOfcr.Text+"', '"+app.Officers.Value(tOfcr.Text)+"', "
		  else
		    ofcr = "'"+OfficerIDSpaceHolder+"', '"+OfficerNameSpaceHolder+"', "
		  end
		  
		  //Counts
		  if ckInit.Value and rbApproved.Value then i=1
		  if ckSupp.Value then j=popSuppCount.ListIndex+1
		  if ckTow.Value then k=popTowCount.ListIndex+1
		  if ckCustody.Value then
		    inCustody = ", 'True', "
		  else
		    inCustody = ", 'False', "
		  end
		  if rbCheckedIn.Value then
		    cnts = "0, 0, 0" + inCustody + str(i) + ", " + str(j) + ", " + str(k) + ", "
		  else
		    cnts = str(i)+", "+str(j)+", "+str(k)+inCustody
		  end
		  
		  //ColumnNames
		  dim col As string = " FileYear, FileNo, FullFile, ReportDate,  "
		  if rbHoldover.Value or rbReturn.Value then col = col + "OfficerID, OfficerName, "
		  col=col+"StatusCode, StatusText, "
		  if not rbVoid.Value then col=col+"Initial, Supp, Tow, InCustody, "
		  if rbCheckedIn.Value then col=col+ "InitialDone, SuppDone, TowDone, "
		  col=col+EntererColumn
		  
		  //Column Values
		  dim value As String = tYear.Text+", "+tFile.Text+", '"+tYear.Text+"-"+tFile.Text+"', '"+RptDateSpaceHolder +"', "
		  if rbHoldover.Value or rbReturn.Value then value = value + ofcr
		  value = value + stat
		  if not rbVoid.Value then value = value + cnts
		  value = value + EntererValue +d.SQLDateTime
		  
		  if rbCheckedIn.Value and not app.CurrentUser.Supervisor then
		    //This is a PSOS trying to do a new checkin.  Normally an error but override allowed
		    i=MsgBox("Filenumber "+tYear.Text+"-"+tFile.Text+" shows no supervisory approval of this report."+_
		    "While it can be checked in without supervisory entry, this may be a wrong number.  Please double check the report number."+_
		    EndOfLine+EndOfLine+"Are you sure you want to check this report into the tracker?" , 4+512, "Possible Incorrect Report Number")
		    if i=7 then //User pushed "No" (default)
		      Return
		    end
		  end
		  
		  ///////  Insert Blanks between new entry and max old entry
		  sql = "SELECT Max(FileNo) FROM Active WHERE FileYear="+tYear.Text +";"
		  rs=db.SQLSelect (sql)
		  if  db.CheckError (sql) then return
		  
		  j = rs.IdxField(1).IntegerValue + 1
		  k = val(tFile.Text)-1
		  if k-j>20 then
		    i=MsgBox("Filenumber "+tYear.Text+"-"+tFile.Text+" is more than 20 reports ahead of the highest-tracked report number."+_
		    "This may be an error.  Please double check the report number."+EndOfLine+EndOfLine+"Are you sure you want to enter "+_
		    str((k-j)+1) + " 'Unknown' entries into the tracker?" , 4+512, "Possible Incorrect Report Number")
		    if i=7 then //User pushed "No" (default)
		      Return
		    end
		  end
		  db.SQLExecute "BEGIN TRANSACTION;"
		  call db.CheckError ("Begin Transaction")
		  for i = j to k
		    sql = "INSERT OR ROLLBACK INTO Active (FileYear, FileNo, FullFile, ReportDate, StatusCode, StatusText) VALUES ("+_
		    tYear.Text+", "+str(i)+", '"+tYear.Text+"-"+str(i)+"', '"+d.SQLDate +"', "+  str(ReportTrackingDB.StatusCode.Unknown) +", 'User Autofill');"
		    db.SQLExecute (sql)
		  next //blank entries
		  
		  
		  ///////  Insert New Entry, which is now also Max(thisYear)
		  sql = "INSERT OR ROLLBACK INTO Active ("+ col +") VALUES (" + value +"');"
		  db.SQLExecute sql
		  call db.CheckError (sql)
		  db.SQLExecute "END TRANSACTION;"
		  call db.CheckError ("End Transaction")
		  
		  //  Set Undo to Missing
		  ReDim UndoFields(UndoCategoryCount)
		  UndoFields(1) = tYear.Text
		  UndoFields(2) = tFile.Text
		  UndoFields(3) = tYear.Text+"-"+tFile.Text
		  UndoFields(4) = RptDateSpaceHolder
		  UndoFields(5) = "0"
		  UndoFields(6) = "UserUndo"
		  pbUndo.Enabled=True
		  
		  RptDateSpaceHolder = ""  //Reset to blank (used to preserve date when overwriting from Update)
		  OfficerIDSpaceHolder = ""
		  OfficerNameSpaceHolder = ""
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function KeyShortcuts(key As String) As Boolean
		  select case asc(lowercase(key))
		  case 1,8,9,28 to 31, 48 to 57, 127
		    return false
		    
		  case 3,13
		    StoreEntry
		  case 97
		    if rbApproved.Enabled then rbApproved.Value = True
		  case 104
		    if rbHoldover.Enabled then rbHoldover.Value = True
		  case 118
		    if rbVoid.Enabled then rbVoid.Value = True
		  case 114
		    if rbReturn.Enabled then rbReturn.Value = True
		  case 115
		    ckSupp.Value = not ckSupp.Value
		  case 105
		    ckInit.Value = not ckInit.Value
		  case 99
		    ckCustody.Value = not ckCustody.Value
		  case 100
		    if rbCheckedIn.Enabled then rbCheckedIn.Value = True
		  case 116
		    ckTow.Value = not ckTow.Value
		  else
		    
		  end select
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ResetFields()
		  tYear.Text=str(app.CurrentYear)
		  
		  if not ckBatchmode.Value then
		    rbCheckedIn.Value=app.CurrentUser.Records
		    rbApproved.Value=app.CurrentUser.Supervisor
		  end
		  
		  ckInit.Value=True
		  ckSupp.Value=false
		  ckTow.Value=False
		  ckCustody.Value=False
		  popSuppCount.ListIndex=0
		  popTowCount.ListIndex=0
		  tFile.CueText =tFile.Text
		  tFile.Text=""
		  tFile.SetFocus
		  
		  if not ckBatchmode.Value then tdUpdateList.Run
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub StoreEntry()
		  dim rs As RecordSet
		  dim sql As string
		  dim db As ReportTrackingDB = app.myDB
		  
		  if not ValidateFields then Return
		  
		  sql = "SELECT * FROM Active WHERE FullFile = '"+tYear.Text+"-"+tFile.Text+"';"
		  if db.Connect then
		    rs=db.SQLSelect (sql)   // Find matching file if it exists
		    if not db.CheckError (sql) then
		      if rs.RecordCount = 0 then  //This is a new entry.
		        InsertRecord
		        
		      else  // We have a match, just edit
		        UpdateRecord (rs)
		        
		        
		      end //File exists
		    end // check error for first search
		  end //connect
		  
		  db.Close
		  ResetFields
		  cameraWav.Play
		  tdUpdateList.Run
		  
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateRecord(rs As RecordSet)
		  //DETERMINE WHICH CATEGORY WE ARE UPDATING FROM
		  dim c as integer=rs.Field("StatusCode").IntegerValue
		  
		  Select Case ReportTrackingDB.StatusCode(c) // OLD STATUS
		  Case ReportTrackingDB.StatusCode.Voided
		    AfterVoid rs
		    
		  Case ReportTrackingDB.StatusCode.ApprovedByWC
		    AfterApproved rs
		    
		  Case ReportTrackingDB.StatusCode.CheckedIntoRecords
		    AfterCheckedIn rs
		    
		  Case ReportTrackingDB.StatusCode.Holdover
		    AfterHoldover rs
		    
		  Case ReportTrackingDB.StatusCode.ReturnedForCorrections
		    AfterKickback rs
		    
		  Case ReportTrackingDB.StatusCode.Unknown
		    // METHODOLOGY :  No info is available.  This report was entered through CPROG (where Date and Officer were entered)
		    //                              or because a higher report number was entered (where only date was estimated).
		    //                              We will overwrite status if initial, officer if listed, and for approved / checked, numbers.
		    
		    
		    dim sql As String
		    dim d as new date
		    dim ofcr, stat, counts as string
		    dim i,k,j as integer
		    if ckInit.Value then i=1
		    If rbHoldover.Value and ckInit.Value then
		      stat = "StatusCode="+str(ReportTrackingDB.StatusCode.Holdover)+", StatusText='Approved to Hold Over', "
		    ElseIf rbApproved.Value and ckInit.Value then
		      stat ="StatusCode="+str(ReportTrackingDB.StatusCode.ApprovedByWC)+", StatusText='Approved to Records', "
		    Elseif rbReturn.Value and ckInit.Value then
		      stat ="StatusCode="+str(ReportTrackingDB.StatusCode.ReturnedForCorrections)+", StatusText='Returned for Corrections', "
		    ElseIf rbCheckedIn.Value and ckInit.Value then
		      stat ="StatusCode="+str(ReportTrackingDB.StatusCode.CheckedIntoRecords)+", StatusText='Checked into Records', "
		    ElseIf rbVoid.Value then
		      stat = "StatusCode="+str(ReportTrackingDB.StatusCode.Voided) + ", StatusText='Voided Report Number', "
		    end
		    if tOfcr.Text<>"" and app.Officers.HasKey(tOfcr.Text) then
		      ofcr="OfficerID="+tOfcr.Text+", OfficerName = '"+app.Officers.Value(tOfcr.Text)+"', "
		    end
		    if ckSupp.Value and (not rbVoid.Value and not rbHoldover.Value) then j=popSuppCount.ListIndex+1
		    if ckTow.Value and (not rbVoid.Value and not rbHoldover.Value) then k=popTowCount.ListIndex+1
		    if rbCheckedIn.Value then
		      counts = "InitialDone="+str(i)+_
		      ", SuppDone="+str(j+rs.Field("SuppDone").IntegerValue)+_
		      ", TowDone="+str(k+rs.Field("TowDone").IntegerValue)+", "
		      counts = counts + "Supp="+str(max(0,rs.Field("Supp").IntegerValue-j))+_
		      ", Tow="+str(max(0,rs.Field("Tow").IntegerValue-k))+", "
		    elseif rbApproved.Value then
		      counts = "Initial="+str(i)+_
		      ", Supp="+str(j+rs.Field("Supp").IntegerValue)+_
		      ", Tow="+str(k+rs.Field("Tow").IntegerValue)+", "
		    end
		    if ckCustody.Value then
		      counts=counts+"InCustody='True', "
		    else
		      counts=Counts+"InCustody='False', "
		    end
		    sql = "UPDATE OR ROLLBACK Active Set "+Stat+Ofcr+Counts+entererUpdate+d.SQLDateTime+_
		    "' WHERE FullFile='" +tYear.Text+"-"+tFile.Text+"';"
		    app.myDB.SQLExecute sql
		    if app.myDB.CheckError (sql) then return
		    
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ValidateFields() As Boolean
		  dim b As Boolean
		  
		  if not rbVoid.Value then
		    b=  ckInit.Value or ckSupp.Value or ckTow.Value
		    if not b then
		      uhohWav.Play
		      MsgBox ("At least one report type must be listed.  Record not Saved.")
		      tFile.SetFocus
		      Return False
		    end
		  end
		  
		  b= val(tYear.Text)>12 and val(tYear.Text)<=App.CurrentYear
		  if not b then
		    uhohWav.Play
		    MsgBox ("Invalid Report Year.  Record not Saved.")
		    tYear.SetFocus
		    return False
		  end
		  
		  b= len(tFile.Text)>0 and val(tFile.Text)<20000
		  if not b then
		    uhohWav.Play
		    MsgBox ("Invalid Report Number.  Record not Saved.")
		    tFile.SetFocus
		    return False
		  end
		  
		  if rbHoldover.Value or rbReturn.Value then
		    if not App.Officers.HasKey(tOfcr.Text) then
		      MsgBox("Officer "+tOfcr.Text+" not found.  Record not Saved.")
		      tOfcr.SetFocus
		      return False
		    end
		  end
		  
		  Return True
		End Function
	#tag EndMethod


	#tag Note, Name = Create Table
		CREATE TABLE Active (FileYear Numeric, FileNo Numeric, FullFile Text, ReportDate Text, 
		StatusCode Numeric, StatusText Text, OfficerID Text, OfficerName Text, Initial Numeric, Supp Numeric, 
		Tow Numeric, InitialDone Numeric, SuppDone Numeric, TowDone Numeric, InCustody Text, LastWC Numeric, 
		WCDateTime Text, LastPSOS Numeric, PSOSDateTime Text);
	#tag EndNote


	#tag Property, Flags = &h0
		cprogProcessed As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		EntererColumn As String
	#tag EndProperty

	#tag Property, Flags = &h0
		entererUpdate As string
	#tag EndProperty

	#tag Property, Flags = &h0
		EntererValue As String
	#tag EndProperty

	#tag Property, Flags = &h0
		MemoryDB As ReportTrackingDB
	#tag EndProperty

	#tag Property, Flags = &h0
		OfficerIDSpaceHolder As string
	#tag EndProperty

	#tag Property, Flags = &h0
		OfficerNameSpaceHolder As string
	#tag EndProperty

	#tag Property, Flags = &h0
		RptDateSpaceHolder As String
	#tag EndProperty

	#tag Property, Flags = &h0
		UndoCategoryCount As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		UndoFields() As String
	#tag EndProperty


#tag EndWindowCode

#tag Events pbEnter
	#tag Event
		Sub Action()
		  StoreEntry
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events tYear
	#tag Event
		Sub Open()
		  dim d as new date
		  me.Text = right(str(d.Year),2)
		End Sub
	#tag EndEvent
	#tag Event
		Function KeyDown(Key As String) As Boolean
		  return KeyShortcuts(key)
		End Function
	#tag EndEvent
	#tag Event
		Sub TextChange()
		  pbEnter.Enabled =tFile.Text<>"" and tYear.Text.Len =2
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events tFile
	#tag Event
		Function KeyDown(Key As String) As Boolean
		  return KeyShortcuts(key)
		End Function
	#tag EndEvent
	#tag Event
		Sub TextChange()
		  pbEnter.Enabled =tFile.Text<>"" and tYear.Text.Len =2
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events tOfcr
	#tag Event
		Sub GotFocus()
		  me.SelectAll
		End Sub
	#tag EndEvent
	#tag Event
		Function KeyDown(Key As String) As Boolean
		  return KeyShortcuts(key)
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events ckInit
	#tag Event
		Sub Action()
		  if not me.Value then InitGoneWav.Play
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ckSupp
	#tag Event
		Sub Action()
		  if me.Value then
		    ckInit.Value = False
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ckTow
	#tag Event
		Sub Action()
		  if me.Value then
		    ckInit.Value = False
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rbApproved
	#tag Event
		Sub Open()
		  me.Enabled = app.CurrentUser.Supervisor
		  me.Value = app.CurrentUser.Supervisor
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  if rbVoid.Value or rbHoldover.Value then
		    pbEnter.Enabled = (ckInit.Value)
		  else
		    pbEnter.Enabled = true
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rbReturn
	#tag Event
		Sub Action()
		  if rbVoid.Value or rbHoldover.Value then
		    pbEnter.Enabled = (ckInit.Value)
		  else
		    pbEnter.Enabled = true
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rbVoid
	#tag Event
		Sub Action()
		  if rbVoid.Value or rbHoldover.Value then
		    pbEnter.Enabled = (ckInit.Value)
		  else
		    pbEnter.Enabled = true
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rbHoldover
	#tag Event
		Sub Open()
		  me.Enabled = app.CurrentUser.Supervisor
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  if rbVoid.Value or rbHoldover.Value then
		    pbEnter.Enabled = (ckInit.Value)
		  else
		    pbEnter.Enabled = true
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ckBatchmode
	#tag Event
		Sub Action()
		  if me.Value then
		    rbHoldover.Value=True
		  else
		    rbApproved.Value=True
		  end
		  rbApproved.Enabled= not me.Value
		  rbVoid.Enabled = not me.Value
		  rbReturn.Enabled = not me.Value
		  tFile.SetFocus
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.Enabled = app.CurrentUser.Supervisor
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events tdUpdateList
	#tag Event
		Sub Run()
		  dim rs as RecordSet
		  dim sql As String
		  dim f As FolderItem
		  dim s As String
		  dim d As new date
		  
		  Prefs.Get("DataBase",f)
		  if f=nil or not f.Exists then return
		  
		  //We have a valid folderitem for disk DB
		  if not MemoryDB.Connect then return
		  MemoryDB.SQLExecute ("DROP TABLE IF EXISTS Search;")
		  if not Memorydb.AttachDatabase(f,"Disk") then return
		  
		  //We now have a blank in-memory and have attached disk.  Copy records necessary to Update Status
		  sql ="CREATE TABLE Active As SELECT FullFile, ReportDate, StatusCode, StatusText, OfficerName, OfficerID, LastWC, WCDateTime, LastPSOS, PSOSDateTime, "+_
		  "Initial, Supp, Tow FROM Disk.Active WHERE StatusCode <> " + str(ReportTrackingDB.StatusCode.Voided) +" AND StatusCode <>"+_
		  str(ReportTrackingDB.StatusCode.CheckedIntoRecords) + ";"
		  
		  MemoryDB.SQLExecute (sql)
		  
		  //Touch Username for keepalive
		  MemoryDB.SQLExecute ("UPDATE Disk.User SET LastTouched = '"+d.SQLDateTime+"' WHERE ID= '" + app.CurrentUser.ID +"';")
		  
		  Memorydb.DetachDatabase ("Disk")
		  
		  
		  // We now have an in-memory database of everything needed to update the status.  Process them
		  
		  rs = MemoryDB.Holdover
		  if rs<>nil then MemoryDB.Msg("Holdover")= "Holdover - "+str(rs.RecordCount)
		  rs = MemoryDB.Corrections
		  if rs<>nil then MemoryDB.Msg("KickBack") = "Kicked Back - "+str(rs.RecordCount)
		  rs = MemoryDB.MissingInfo(true)
		  if rs<>nil then
		    s = "Unknown - ("+str(rs.RecordCount)+") "
		    rs = MemoryDB.MissingInfo(False)
		    if rs<>nil then MemoryDB.Msg("Unknown") = s +str(rs.RecordCount)
		  end
		  rs = MemoryDB.MonthOld
		  if rs<>nil then MemoryDB.Msg("One Month") = "Over 1 Month - "+str(rs.RecordCount)
		  rs = MemoryDB.TwoWeeksOld
		  if rs<>nil then MemoryDB.Msg("Two Weeks")  = "Over 2 Weeks - "+str(rs.RecordCount)
		  rs = MemoryDB.Approved(true)
		  if rs<>nil then
		    s = "Not Checked - ("+str(rs.RecordCount)+") "
		    rs = MemoryDB.Approved(False)
		    if rs<>nil then MemoryDB.Msg("Not Checked")  = s +str(rs.RecordCount)
		  end
		  
		  
		  MemoryDB.Close
		  
		  MemoryDB.UIChangeFlag=True
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TimerGUI
	#tag Event
		Sub Action()
		  if not MemoryDB.UIChangeFlag then return
		  dim m As ReportTrackingDB = MemoryDB
		  dim b As string = m.Msg("Error")
		  
		  if b="True" then
		    dim s1 as string = m.Msg("SQL Command")
		    dim s2 As String = m.Msg("Error Message")
		    
		    MsgBox("A Database error occured updating the Outstanding Report Counts.    "+ EndOfLine +_
		    "This is not harmful, but the information may be inaccurate"+EndOfLine+_
		    "Command: "+s1+EndOfLine + "Error Message: "+s2+")")
		    
		    m.Msg("Error")="False"
		  else
		    btnHoldover.Caption = m.Msg("Holdover")
		    btnKickback.Caption = m.Msg("KickBack")
		    btnMissing.Caption =m.Msg("Unknown")
		    btnOneMonth.Caption = m.Msg("One Month")
		    btnTwoWeeks.Caption = m.Msg("Two Weeks")
		    btnWaitingCheckin.Caption = m.Msg("Not Checked")
		  end
		  
		  m.UIChangeFlag = False
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TimerDBHit
	#tag Event
		Sub Action()
		  tdUpdateList.Run
		  #if TargetWin32
		    if not cprogProcessed then
		      CPROGDownload True
		    end
		  #endif
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnOneMonth
	#tag Event
		Sub Open()
		  dim p As new Picture(me.Width-6,me.Height-6,32)
		  dim g As Graphics = p.Graphics
		  
		  #if TargetMacOS then
		    g.ForeColor= me.BackColor
		    g.FillRect(0,0,p.Width,p.Height)
		    me.Icon = p
		  #endif
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  dim t As string= "Reports Over One Month Old"
		  
		  me.Enabled = false // disable multiple clicks
		  dim rs As RecordSet
		  dim w As window
		  dim n as string
		  
		  // Set up wListbox headings, column widths, title
		  Prefs.Get (t+" FieldNames",n)
		  if n = "" then
		    Prefs.Set( t+" FieldNames", array("Report Number", "Date","Status", "Officer","Last Approved by","Date/Time"))
		    Prefs.Set( t+" ColumnWidths","200,100,200,50,50,200")
		  end
		  Prefs.Set("ListReport Title", t)
		  
		  
		  rs = app.myDB.MonthOld
		  if rs=nil then
		    MsgBox ("Database Error.  Unable to generate report.")
		    
		  else
		    
		    w = app.GetWindowByName (t)
		    if w= nil then
		      w= new wListbox
		      w.Title = t
		    end
		    
		    wListbox(w).ListboxUpdate (rs)
		    w.Show
		  end
		  
		  me.Enabled = True
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnTwoWeeks
	#tag Event
		Sub Open()
		  dim p As new Picture(me.Width-6,me.Height-6,32)
		  dim g As Graphics = p.Graphics
		  
		  #if TargetMacOS then
		    g.ForeColor= me.BackColor
		    g.FillRect(0,0,p.Width,p.Height)
		    me.Icon = p
		  #endif
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  dim t As string= "Reports Over Two Weeks Old"
		  
		  me.Enabled = false // disable multiple clicks
		  
		  dim rs As RecordSet
		  dim w As window
		  dim n as string
		  
		  // Set up wListbox headings, column widths, title
		  Prefs.Get (t+" FieldNames",n)
		  if n = "" then
		    Prefs.Set( t+" FieldNames", array("Report Number", "Date","Status","Officer","Last Approved by","Date/Time"))
		    Prefs.Set( t+" ColumnWidths","200,100,200,50,50,200")
		  end
		  Prefs.Set("ListReport Title", t)
		  
		  
		  rs = app.myDB.TwoWeeksOld
		  if rs=nil then
		    MsgBox ("Database Error.  Unable to generate report.")
		    
		  else
		    
		    w = app.GetWindowByName (t)
		    if w= nil then
		      w= new wListbox
		      w.Title = t
		    end
		    
		    wListbox(w).ListboxUpdate (rs)
		    w.Show
		  end
		  
		  me.Enabled = True
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnWaitingCheckin
	#tag Event
		Sub Open()
		  dim p As new Picture(me.Width-6,me.Height-6,32)
		  dim g As Graphics = p.Graphics
		  
		  #if TargetMacOS then
		    g.ForeColor= me.BackColor
		    g.FillRect(0,0,p.Width,p.Height)
		    me.Icon = p
		  #endif
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  dim t As string= "Approved for Records Checkin"
		  me.Enabled = false // disable multiple clicks
		  
		  dim rs As RecordSet
		  dim w As window
		  dim n as string
		  
		  // Set up wListbox headings, column widths, title
		  Prefs.Get (t+" FieldNames",n)
		  if n = "" then
		    Prefs.Set( t+" FieldNames", array("Report Number", "Date","Officer","Last Approved by","Date/Time","Initial", "Supp", "Tow"))
		    Prefs.Set( t+" ColumnWidths","200,200,50,50,200,40,40,40")
		  end
		  Prefs.Set("ListReport Title", t)
		  
		  
		  rs = app.myDB.Approved(Keyboard.ShiftKey or app.CurrentUser.Records)
		  if rs=nil then
		    MsgBox ("Database Error.  Unable to generate report.")
		    
		  else
		    
		    w = app.GetWindowByName (t)
		    if w= nil then
		      w= new wListbox
		      w.Title = t
		    end
		    
		    wListbox(w).ListboxUpdate (rs)
		    w.Show
		  end
		  
		  me.Enabled = True
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnKickback
	#tag Event
		Sub Open()
		  dim p As new Picture(me.Width-6,me.Height-6,32)
		  dim g As Graphics = p.Graphics
		  
		  #if TargetMacOS then
		    g.ForeColor= me.BackColor
		    g.FillRect(0,0,p.Width,p.Height)
		    me.Icon = p
		  #endif
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  dim t As string= "Returned for Corrections"
		  
		  me.Enabled = false // disable multiple clicks
		  dim rs As RecordSet
		  dim w As window
		  dim n as string
		  
		  // Set up wListbox headings, column widths, title
		  Prefs.Get (t+" FieldNames",n)
		  if n = "" then
		    Prefs.Set( t+" FieldNames", array("Report Number", "Date","Officer","Last Approved by","Date/Time","Records Clerk","Records Date/Time"))
		    Prefs.Set( t+" ColumnWidths","200,200,200,200,200,200,200")
		  end
		  Prefs.Set("ListReport Title", t)
		  
		  
		  rs = app.myDB.Corrections
		  if rs=nil then
		    MsgBox ("Database Error.  Unable to generate report.")
		    
		  else
		    
		    w = app.GetWindowByName (t)
		    if w= nil then
		      w= new wListbox
		      w.Title = t
		    end
		    
		    wListbox(w).ListboxUpdate (rs)
		    w.Show
		  end
		  
		  me.Enabled = True
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnHoldover
	#tag Event
		Sub Open()
		  dim p As new Picture(me.Width-6,me.Height-6,32)
		  dim g As Graphics = p.Graphics
		  
		  #if TargetMacOS then
		    g.ForeColor= me.BackColor
		    g.FillRect(0,0,p.Width,p.Height)
		    me.Icon = p
		  #endif
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  dim t As string= "Reports Approved for Holdover"
		  
		  me.Enabled = false // disable multiple clicks
		  dim rs As RecordSet
		  dim w As window
		  dim n as string
		  
		  // Set up wListbox headings, column widths, title
		  Prefs.Get (t+" FieldNames",n)
		  if n = "" then
		    Prefs.Set( t+" FieldNames", array("Report Number", "Date","Officer","Approved by","Date/Time"))
		    Prefs.Set( t+" ColumnWidths","200,200,200,200,200")
		  end
		  Prefs.Set("ListReport Title", t)
		  
		  
		  rs = app.myDB.Holdover
		  if rs=nil then
		    MsgBox ("Database Error.  Unable to generate report.")
		    
		  else
		    
		    w = app.GetWindowByName (t)
		    if w= nil then
		      w= new wListbox
		      w.Title = t
		    end
		    
		    wListbox(w).ListboxUpdate (rs)
		    w.Show
		  end
		  
		  me.Enabled = True
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnMissing
	#tag Event
		Sub Open()
		  dim p As new Picture(me.Width-6,me.Height-6,32)
		  dim g As Graphics = p.Graphics
		  
		  #if TargetMacOS then
		    g.ForeColor= me.BackColor
		    g.FillRect(0,0,p.Width,p.Height)
		    me.Icon = p
		  #endif
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  me.Enabled = false // disable multiple clicks
		  
		  dim rs As RecordSet
		  dim t As string= "Reports Without Status Info"
		  dim w As window
		  dim n as string
		  
		  // Set up wListbox headings, column widths, title
		  Prefs.Get (t+" FieldNames",n)
		  if n = "" then
		    Prefs.Set( t+" FieldNames", array("Report Number","Possible Report Date","Officer"))
		    Prefs.Set( t+" ColumnWidths","200,200,200")
		  end
		  Prefs.Set("ListReport Title", t)
		  
		  
		  rs = app.myDB.MissingInfo(Keyboard.ShiftKey)
		  if rs=nil then
		    MsgBox ("Database Error.  Unable to generate report.")
		    
		  else
		    
		    w = app.GetWindowByName (t)
		    if w= nil then
		      w= new wListbox
		      w.Title = t
		    end
		    
		    wListbox(w).ListboxUpdate (rs)
		    w.Show
		  end
		  
		  me.Enabled = True
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnFilter
	#tag Event
		Sub Open()
		  Dim months() As String = Array("All", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
		  
		  For Each m As String In months
		    Me.AddRow(m)
		  Next
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  'Me.Caption = "Filter: "+Me.List(Me.MenuValue)
		  
		  'tdUpdateList.Run
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rbCheckedIn
	#tag Event
		Sub Open()
		  if App.CurrentUser.Records and not app.CurrentUser.Supervisor then
		    me.Value = True
		  end
		  me.Enabled = app.CurrentUser.Records
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  if rbVoid.Value or rbHoldover.Value then
		    pbEnter.Enabled = (ckInit.Value)
		  else
		    pbEnter.Enabled = true
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbUndo
	#tag Event
		Sub Action()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbSearch
	#tag Event
		Sub Action()
		  if tFile.Text<>"" and tOfcr.Text<>"" then
		    MsgBox ("You can only search by file number OR officer ID."+EndOfLine+EndOfLine+_
		    "Please delete the file number to create an officer report, or delete the officer to get a file number report")
		    Return
		  end
		  
		  if tFile.Text<>"" and tYear.Text="" then
		    MsgBox ("Year is blank.  Please enter a year")
		    Return
		  end if
		  
		  me.Enabled = false // disable multiple clicks
		  dim fields() As String
		  fields = array("File Number", "Date Reported","Officer Assigned","Overall Status (Initial)", "Last WC","WC Date/Time","Records Entry By","Records Date/Time",_
		  "In Custody?", "Initial at Approved Stage", "Initial Checked in by Records", "Supp(s) Approved","Supps Checked In", _
		  "Tow(s) Approved", "Tow(s) Checked In")
		  
		  dim t As string= "File Number Report"
		  
		  dim rs As RecordSet
		  dim w As window
		  dim n as string
		  
		  
		  // Set up wListbox headings, column widths, title
		  Prefs.Get (t+" FieldNames",n)
		  if n = "" then
		    Prefs.Set( t+" FieldNames", Array("",""))
		    Prefs.Set( t+" ColumnWidths","200,200")
		  end
		  Prefs.Set("ListReport Title", t+"- "+tYear.Text+"-"+tFile.Text)
		  
		  
		  rs = app.myDB.Corrections
		  if rs=nil then
		    MsgBox ("Database Error.  Unable to generate report.")
		    
		  else
		    
		    w = app.GetWindowByName (t)
		    if w= nil then
		      w= new wListbox
		      w.Title = t
		    end
		    
		    wListbox(w).ListboxUpdate (rs)
		    w.Show
		  end
		  
		  me.Enabled = True
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="cprogProcessed"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EntererColumn"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="entererUpdate"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EntererValue"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="OfficerIDSpaceHolder"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="OfficerNameSpaceHolder"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="RptDateSpaceHolder"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UndoCategoryCount"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
