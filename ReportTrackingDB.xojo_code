#tag Class
Class ReportTrackingDB
Inherits SQLiteDatabase
	#tag Method, Flags = &h0
		Function Approved(IncludeNew as Boolean = True) As RecordSet
		  // includeNew returns all matches, while false returns reports greater than one day old, not counting weekends
		  
		  dim d As new date
		  dim i As Integer
		  dim rs As RecordSet
		  dim sql as String
		  
		  if IncludeNew then
		    sql= "SELECT FullFile, ReportDate, OfficerID, OfficerName, LastWC, WCDateTime, Initial, Supp, Tow FROM "+_
		    "Active WHERE StatusCode = "+str(StatusCode.ApprovedByWC)
		  else
		    select case d.DayOfWeek
		    case 3,4,5,6
		      i=-1
		    case 7
		      i=-2
		    case 1,2
		      i=-2-d.DayOfWeek
		    end
		    sql= "SELECT FullFile, ReportDate, OfficerID, OfficerName, LastWC, WCDateTime, Initial, Supp, Tow FROM " +_
		    "Active WHERE StatusCode = "+str(StatusCode.ApprovedByWC)+ " AND WCDateTime < date('now', '"+str(i)+" days')"
		  end
		  
		  if not me.InMemory then
		    if not me.Connect then return nil
		  end
		  rs = me.SQLSelect (sql)
		  
		  if me.CheckError (sql) then Return nil
		  if not me.InMemory then me.Close  //We don't open or close in-memory DBs
		  
		  return rs
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CheckError(search as string, fatal as boolean = false, retries as integer = 2) As Boolean
		  if not me.Error then return False
		  
		  if app.CurrentThread = nil then
		    
		    dim s As string
		    Beep
		    if fatal then
		      s="The tracker must quit.  Please contact your system administrator. ("
		    else
		      s="No changes were made to the database. ("
		    end
		    
		    MsgBox "A database error occurred with command """ +search+""" " +  EndOfLine + EndOfLine + s +_
		    me.ErrorMessage+", "+str(me.ErrorCode)+ ")"
		    if fatal then app.ForceQuit
		    
		  Else
		    Msg("Error") = "True"
		    Msg("SQL Command") = search
		    Msg("Error Message") = me.ErrorMessage + ", "+str(me.ErrorCode)
		  end
		  UIChangeFlag=True
		  
		  return True
		  
		  // beta  Do all error handling here, include retries
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Corrections() As RecordSet
		  dim rs As RecordSet
		  dim sql as String = "SELECT FullFile, ReportDate, OfficerID, OfficerName, LastWC, WCDateTime, LastPSOS, PSOSDateTime FROM "+_
		  "Active WHERE StatusCode = "+str(StatusCode.ReturnedForCorrections)
		  
		  
		  if not me.InMemory then
		    if not me.Connect then return nil
		  end
		  rs = me.SQLSelect (sql)
		  
		  if me.CheckError (sql) then Return nil
		  if not me.InMemory then me.Close  //We don't open or close in-memory DBs
		  
		  return rs
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Holdover() As RecordSet
		  dim rs As RecordSet
		  dim sql as String = "SELECT FullFile, ReportDate, OfficerID, OfficerName, LastWC, WCDateTime "+_
		  "FROM Active WHERE StatusCode = "+str(StatusCode.Holdover)
		  
		  if not me.InMemory then
		    if not me.Connect then return nil
		  end
		  rs = me.SQLSelect (sql)
		  
		  if me.CheckError (sql) then Return nil
		  if not me.InMemory then me.Close  //We don't open or close in-memory DBs
		  
		  return rs
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function InMemory() As Boolean
		  Return me.DatabaseFile = nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MissingInfo(IncludeNew as boolean = true) As RecordSet
		  // includeNew returns all matches, while false returns reports greater than one day old
		  
		  dim rs As RecordSet
		  dim sql as String
		  
		  if IncludeNew then
		    sql= "SELECT FullFile, ReportDate, OfficerID, OfficerName FROM Active WHERE StatusCode = "+str(StatusCode.Unknown)
		  else
		    sql= "SELECT FullFile, ReportDate, OfficerID, OfficerName FROM Active WHERE StatusCode = "+str(StatusCode.Unknown)+ " AND ReportDate < date('now','-1 days')"
		  end
		  
		  
		  if not me.InMemory then
		    if not me.Connect then return nil
		  end
		  rs = me.SQLSelect (sql)
		  
		  if me.CheckError (sql) then Return nil
		  if not me.InMemory then me.Close  //We don't open or close in-memory DBs
		  
		  return rs
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MonthOld() As RecordSet
		  dim rs As RecordSet
		  dim sql as String
		  
		  sql = "SELECT FullFile, ReportDate, StatusText, OfficerID, OfficerName, LastWC, WCDateTime FROM Active "+_
		  "WHERE StatusCode <"+str(StatusCode.Voided)+" AND ReportDate <= date('now','-1 months')"
		  
		  
		  if not me.InMemory then
		    if not me.Connect then return nil
		  end
		  rs = me.SQLSelect (sql)
		  
		  if me.CheckError (sql) then Return nil
		  if not me.InMemory then me.Close  //We don't open or close in-memory DBs
		  
		  return rs
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Msg(key as string) As String
		  if mMessages = nil then mMessages=new Dictionary
		  dim s As String
		  
		  if mMessages.HasKey(key) then
		    s= mMessages.Value(Key)
		    return s
		  Else
		    Return ""
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Msg(Key as string, Assigns Value As String)
		  if mMessages = nil then mMessages=new Dictionary
		  
		  mMessages.Value(key)=Value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TwoWeeksOld() As RecordSet
		  dim rs As RecordSet
		  dim sql as String
		  
		  sql = "SELECT FullFile, ReportDate, StatusText, OfficerID, OfficerName, LastWC, WCDateTime FROM "+_
		  "Active WHERE StatusCode <"+str(StatusCode.Voided)+" AND ReportDate < date('now','-14 days')"
		  
		  
		  if not me.InMemory then
		    if not me.Connect then return nil
		  end
		  rs = me.SQLSelect (sql)
		  
		  if me.CheckError (sql) then Return nil
		  if not me.InMemory then me.Close  //We don't open or close in-memory DBs
		  
		  return rs
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private mMessages As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		UIChangeFlag As Boolean
	#tag EndProperty


	#tag Enum, Name = StatusCode, Type = Integer, Flags = &h0
		Unknown
		  Holdover
		  ReturnedForCorrections
		  ApprovedByWC
		  Voided
		CheckedIntoRecords
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="DatabaseFile"
			Visible=true
			Type="FolderItem"
			EditorType="FolderItem"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DebugMode"
			Visible=true
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EncryptionKey"
			Visible=true
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MultiUser"
			Visible=true
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShortColumnNames"
			Visible=true
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ThreadYieldInterval"
			Visible=true
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Timeout"
			Visible=true
			Type="Double"
			EditorType="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UIChangeFlag"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
