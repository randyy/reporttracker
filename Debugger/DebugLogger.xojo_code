#tag Module
Protected Module DebugLogger
	#tag Method, Flags = &h0
		Function DLog() As Logger
		  
		  // This routine returns the object that stores, and
		  // provides an API to, our DebugLogger.
		  
		  // If we don't have a Logger object, make one.
		  // (This is so that our caller doesn't have to worry
		  // about it -- he just calls this routine.)
		  
		  if _DLog = nil then
		    _DLog = new Logger(LogFile)
		  end if
		  
		  return _DLog
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function LogFile() As folderItem
		  // Returns a folderItem representing a file within the
		  // Debug Log that is appropriate for the given
		  // application name and Runtime.
		  
		  dim FileName As string
		  
		  if DLogFileName=""  or DebuggingNow then  //Allows App to change default name
		    
		    if AppName = "" then
		      MsgBox "Error - "+AppName+" Debug Log File was called with an empty application name."
		    end if
		    
		    dim d As new Date
		    FileName = "Debug "+AppName+" "+d.SQLDate+" "+str(d.hour)+" "+str(d.Minute)+" "+str(d.Second)
		  else
		    FileName=DLogFileName
		  end
		  
		  // Shrink the file name as appropriate to the platform.
		  dim f As FolderItem
		  dim maxLength as integer = 255
		  if DLogRootFolder=nil or DebuggingNow then // Allows App to change default location
		    f= new FolderItem
		  else
		    f=DLogRootFolder
		  end
		  
		  #if TargetPPC or Target68K
		    maxLength = 31 // OS 9 only allows 31 chars
		  #endif
		  
		  if FileName.Len > maxLength then
		    FileName = AppName + " DBug"
		  end if
		  
		  if FileName.Len > maxLength then
		    FileName = "DebugFile"
		  end if
		  
		  FileName = FileName.Left(maxLength)
		  
		  f=f.Child(FileName)
		  
		  return f
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		DLogFileName As string
	#tag EndProperty

	#tag Property, Flags = &h0
		DLogRootFolder As folderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		_DLog As Logger
	#tag EndProperty


	#tag Constant, Name = DebuggingNow, Type = Boolean, Dynamic = False, Default = \"False", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="DLogFileName"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
