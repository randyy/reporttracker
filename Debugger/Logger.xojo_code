#tag Class
Protected Class Logger
	#tag Method, Flags = &h0
		Sub Append(Reason As string = "Blank", Content As string = "")
		  dim t As TextOutputStream
		  dim d As new Date
		  
		  if LastFileUsed<>nil then
		    t=LastFileUsed.AppendToTextFile
		    t.WriteLine (Reason+Separator+Content)
		    t.Close
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(file as FolderItem = nil)
		  
		  LastFileUsed = file // remember for later
		  
		  if file <> nil then
		    
		    me.Delete (file)
		    me.TimeStamp
		    me.Append("File Created")
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Delete(file as folderitem = nil)
		  if file = nil then
		    file = LastFileUsed // use what we used last time
		  else
		    LastFileUsed = file // remember for later
		  end if
		  
		  if file <> nil then
		    
		    // Here we delete our preference file.
		    file.Delete
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Separator() As string
		  
		  // This separates values from names, and also from
		  // other values in cases where we store multiple
		  // values per name.
		  //
		  // A tab character is chosen because it is compatible
		  // with multiple filenames and multiple numbers on a
		  // single line. It does limit string parameters to
		  // one value per name. (This might relax someday if
		  // we update to respect quotes or some such thing.)
		  
		  return TAB
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TimeStamp()
		  dim t As TextOutputStream
		  dim d As new Date
		  
		  if LastFileUsed<>nil then
		    t=LastFileUsed.AppendToTextFile
		    t.Write d.SQLDateTime+"-"+Separator
		    t.Close
		  end
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private LastFileUsed As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private _Content As String
	#tag EndProperty


	#tag Constant, Name = HexDigits, Type = String, Dynamic = False, Default = \"0123456789ABCDEF", Scope = Private
	#tag EndConstant

	#tag Constant, Name = Suffix_WindowPos, Type = String, Dynamic = False, Default = \"_LeftTopWidthHeight", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
