#tag Window
Begin Window wListbox
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   600
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Untitled"
   Visible         =   True
   Width           =   800
   Begin PushButton PushButton1
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Close"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   700
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "SmallSystem"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   560
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton PushButton3
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Print"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "SmallSystem"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   560
      Underline       =   False
      Visible         =   False
      Width           =   80
   End
   Begin PushButton PushButton4
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Excel Export"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   112
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "SmallSystem"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   560
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Listbox Listbox1
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   12
      ColumnsResizable=   True
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   True
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   534
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "File Number	Date Taken	Officer	Status	Initial	Supp	Tow	In Custody?	Supervisor	Time	PSOS	Time"
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   True
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "SmallSystem"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   760
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Close()
		  dim name() As String
		  dim columns() As String
		  dim col as string
		  
		  dim c as integer = Listbox1.ColumnCount-1
		  for i as integer= 0 to c
		    name.Append (Listbox1.Heading(i))
		    columns.Append str(Listbox1.Column(i).WidthActual)
		  next
		  col = join(columns,", ")
		  
		  dim t as string = self.Title  //  T is used because officer reports and file number reports list the officer/file in the title.  This strips it.
		  dim dash as integer = t.InStr("-")
		  if dash<>0 then t=left(t,dash-1)
		  
		  prefs.set(t+" FieldNames", name )
		  prefs.set(t+" ColumnWidths" , col)
		  
		  Prefs.SetWindowPos(t,me)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  dim s As String
		  dim t As String
		  dim head() as String
		  
		  Prefs.Get("ListReport Title", s)
		  me.Title = s
		  t=s
		  dim dash as integer = t.InStr("-")
		  if dash<>0 then t=left(t,dash-1)  // dash found.  Strip it and everthing that follows
		  
		  Prefs.Get(t+" FieldNames", s)
		  head=s.Split(chr(9))
		  Listbox1.ColumnCount=UBound(head)+1
		  Listbox1.Heading(-1) = s
		  Prefs.Get(t+" ColumnWidths", s)
		  Listbox1.ColumnWidths = s
		  
		  Prefs.GetWindowPos (t, me)
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ListboxUpdate(rs as recordset)
		  dim l As Listbox = me.Listbox1
		  dim i as integer = l.ColumnCount-1
		  dim pos as integer
		  
		  
		  l.DeleteAllRows
		  rs.MoveFirst
		  pos=1
		  while not rs.EOF
		    l.AddRow (" ")
		    For j As Integer = 0 To i
		      if l.Heading(j)= "Officer" then
		        if rs.IdxField(pos).StringValue<>"" then
		          l.Cell(l.LastIndex,j) = rs.IdxField(pos+1).StringValue +" ("+rs.IdxField(pos).StringValue+")"
		        end
		        pos=pos+2
		        
		      else
		        l.Cell(l.LastIndex, j) = rs.IdxField(pos).StringValue
		        pos=pos+1
		      end
		    Next
		    pos=1
		    rs.MoveNext
		  wend
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetColumns(ParamArray ColumnNames as string)
		  dim i As integer
		  dim s() as String
		  dim t As String
		  
		  for Each t in ColumnNames
		    s.Append (t)
		  next
		  
		  Listbox1.ColumnCount = s.Ubound+1
		  for i = 0 to s.UBound
		    Listbox1.Heading(i)=s(i)
		  next
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetColumnWidths(paramarray widths as integer)
		  dim i As Integer
		  dim w() As String
		  dim s As String
		  
		  for each i in widths
		    w.Append str(i)
		  next
		  
		  i =min(Listbox1.ColumnCount-1,w.Ubound)
		  
		  s=join(w,",")
		  Listbox1.ColumnWidths = s
		End Sub
	#tag EndMethod


#tag EndWindowCode

#tag Events PushButton1
	#tag Event
		Sub Action()
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events PushButton3
	#tag Event
		Sub Action()
		  MsgBox ("This feature is not yet implemented.")
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events PushButton4
	#tag Event
		Sub Action()
		  #if TargetMacOS then
		    MsgBox("This function only works on Windows")
		    
		  #else
		    
		    Dim excel As New ExcelApplication
		    Dim book As ExcelWorkbook
		    Dim sheet As ExcelWorksheet
		    dim r As ExcelRange
		    dim found as Boolean
		    
		    excel.Visible = True
		    
		    for i as integer = 1 to excel.Workbooks.Count-0
		      book = excel.Workbooks(i)
		      for j as integer =1 to book.Sheets.Count
		        sheet = book.Sheets(j)
		        if sheet.Name= self.Title then
		          found=True
		          exit
		        end
		      next
		    next
		    if not found then
		      book = excel.Workbooks.Add
		      book.Title = self.Title
		      excel.ActiveSheet.Name = self.Title
		      
		    else
		      //book represents CPROG
		      Sheet = book.ActiveSheet
		      Sheet.Activate
		      r=excel.Cells
		      r.Delete
		    end
		    
		    
		    for j as integer = 0 to Listbox1.ColumnCount-1
		      For i As Integer = 0 To listbox1.listcount - 1
		        excel.Range(chr(65+j) + Str(i + 2), chr(65+j) + Str(i + 2)).Value = listBox1.Cell(i, j)  //Caution, only 26 columns allowed
		      Next
		    next
		    for i as integer = 0 to listbox1.ColumnCount-1
		      excel.Range(chr(65+i)+"1").Value = Listbox1.Heading(i)
		    next
		    
		    r=excel.Range("A1:"+chr(Listbox1.ColumnCount+64)+"1")
		    r.Select_
		    r.Interior.ColorIndex=1 //black
		    r.Font.Bold = True
		    r.Font.ColorIndex =2 //white
		    
		    r=excel.Cells
		    r.Select_
		    r.Font.Size = 9
		    r.AutoFilter
		    
		    // for i as integer = 1 to Listbox1.columncount
		    // excel.Columns(chr(Listbox1.ColumnCount+64)+":"+chr(Listbox1.ColumnCount+64)).EntireColumn.AutoFit
		    // next
		  #endif
		  
		Exception err as OLEException
		  MsgBox err.message
		  
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
