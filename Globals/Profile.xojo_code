#tag Class
Class Profile
	#tag Method, Flags = &h0
		Function ReadProfile(theRecord as recordset) As Boolean
		  if theRecord.RecordCount<>1 then return false
		  theRecord.MoveFirst
		  me.Username = theRecord.Field("Username").StringValue
		  me.Supervisor = theRecord.Field("Supervisor").BooleanValue
		  me.Administrator = theRecord.Field("Administrator").BooleanValue
		  me.Records = theRecord.Field("Records").BooleanValue
		  me.ID= theRecord.Field("ID").StringValue
		  me.DefaultGroup = theRecord.Field("ParentGroup").StringValue
		  me.Name = theRecord.Field("FirstName").StringValue+" "+theRecord.Field("LastName").StringValue
		  me.TempPassword = theRecord.Field("TempPassword").StringValue
		  me.passHash= theRecord.Field("PassHash").StringValue
		  if theRecord.Field("LastTouched").StringValue<>"" then
		    me.LastTouched = new date
		    try
		      me.LastTouched.SQLDateTime = theRecord.Field("LastTouched")
		    Catch err as UnsupportedFormatException
		      me.LastTouched = nil
		    end
		  end
		  Return True
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ValidPassword(password as string) As Boolean
		  dim hash As String
		  
		  if TempPassword<>"" then
		    return (password=TempPassword)
		  Else
		    hash = Crypto.PBKDF2("Dragon'sLair", password, 5029, 64, Crypto.Algorithm.SHA512)
		    hash = DefineEncoding(hash, Encodings.UTF16)
		    Return hash = passHash
		  end
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		Administrator As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		DefaultGroup As string
	#tag EndProperty

	#tag Property, Flags = &h0
		ID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		LastTouched As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		Name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		passHash As string
	#tag EndProperty

	#tag Property, Flags = &h0
		Records As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Supervisor As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		TempPassword As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Username As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Administrator"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DefaultGroup"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="passHash"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Records"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Supervisor"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TempPassword"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
