#tag Module
Protected Module MiscHelpers
	#tag Method, Flags = &h0
		Function CR() As string
		  return chr(13)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LF() As string
		  return chr(10)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Now() As Date
		  
		  // This is so you can say things like:
		  //
		  //   dim s as string = Now.LongDate
		  //
		  // without having to dim a separate variable for the date.
		  
		  return new Date
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Str(Bool as boolean) As String
		  if Bool then
		    return "True"
		  else
		    Return "False"
		  end
		End Function
	#tag EndMethod


	#tag Constant, Name = NA, Type = Integer, Dynamic = False, Default = \"-2147483648", Scope = Public
	#tag EndConstant

	#tag Constant, Name = NL, Type = String, Dynamic = False, Default = \"\r", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"\r\n"
	#tag EndConstant

	#tag Constant, Name = PI, Type = Double, Dynamic = False, Default = \"3.14159265358979323846264338327950", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TAB, Type = String, Dynamic = False, Default = \"\t", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
