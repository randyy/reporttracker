#tag Module
Protected Module AppStrings
	#tag Method, Flags = &h0
		Function AppCopyright() As string
		  const token = "©"
		  return token + pAppVersion_Long.NthField(token, 2)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AppVersion_Long() As string
		  return pAppVersion_Long.NthField(" ", 1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ReplaceAppStrings(extends s as string) As string
		  
		  // This routine does replacements of standard string slugs
		  // with the appropriate string at runtime. This way you can
		  // construct strings in the UI or in a resource file with
		  // generic slugs, then call this routine and have them all
		  // replaced at once.
		  //
		  // This method is within AppConstants because it is expected
		  // that several standard strings will arise in that area...
		  
		  if s.InStr("{") > 0 then // an optimization
		    
		    s = s.ReplaceAll("{AppName}", AppName)
		    s = s.ReplaceAll("{AppVersion}", AppVersion_Short)
		    s = s.ReplaceAll("{AppVersion_Short}", AppVersion_Short)
		    s = s.ReplaceAll("{AppVersion_Long}", AppVersion_Long)
		    s = s.ReplaceAll("{AppCopyright}", AppCopyright)
		    s = s.ReplaceAll("{AppEmail}", AppEmail)
		    
		  end if
		  
		  return s
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReplaceAppStrings(extends w as Window)
		  
		  // This routine does standard string replacement for
		  // all controls within a window that typically benefit
		  // from it, and also for the window's title bar.
		  //
		  // With this routine, you don't need to worry about
		  // individual item replacement; just call this in the
		  // window's Open event and you're all set.
		  
		  w.Title = w.Title.ReplaceAppStrings
		  
		  dim i as integer
		  for i = 0 to w.ControlCount-1
		    
		    if w.Control(i) isA StaticText then
		      StaticText(w.Control(i)).Text _
		      = StaticText(w.Control(i)).Text.ReplaceAppStrings
		    end if
		    
		    if w.Control(i) isA EditField then
		      EditField(w.Control(i)).Text _
		      = EditField(w.Control(i)).Text.ReplaceAppStrings
		    end if
		    
		  next i
		  
		End Sub
	#tag EndMethod


	#tag Note, Name = About this module
		
		This module contains constants -- and some closely related
		methods -- that are used in the Build Settings dialog, and
		in certain UI elements (the About box, for example). They
		are collected into this module so that they can be seen
		together, and easily reviewed for consistency and accuracy.
		
		Most of the items are self-explanatory, but some deserve
		special mention:
		
		APPLICATION NAME
		
		AppName is used within the application to refer to itself,
		independent of platform. For example, instead of using the
		application's filename, as in:
		
		  "CrazyCal.exe must quit now."
		
		it's better to show this to a user:
		
		  "Crazy Calendar must quit now."
		
		Other places that use this info: preference file name, menu
		item titles, window titles...
		
		Note that "AppFileName_[platform]" are also available here,
		for easy review. You can use them in code to get the file
		name of the executable, but it's probably better to get it
		from the application's FolderItem, in case the executable
		file has been renamed.
		
		VERSION INFORMATION
		
		Different operating systems make use of the version
		information fields in Build Settings in different ways:
		
		- On Macintosh, only LongVersion and PackageInfo are used.
		- LongVersion is "Copyright" on Windows, "Version" on Mac.
		- PackageInfo is "Description" on Windows, unlabelled on Mac.
		
		I like my file information windows to show the copyright, URL,
		and complete version number. Given the way the components are
		displayed, I think the best cross-platform result comes from
		putting the full version and copyright into LongVersion, and
		putting the URL into PackageInfo. This necessitates a few
		compromises:
		
		- long version and copyright are extracted at runtime
		  via methods in this module
		- version number (four numbers plus release stage) must
		  be maintained both in this module and in Build settings
		- _AppVersion_Long constant is public (should be private,
		  but Build Settings won't recognize it in that case)
		
		An alternative is to use platform-specific variants for the
		various strings, but then they can't be reviewed at a glance.
		One hopes that REALbasic will someday offer:
		
		- the ability to define constants in terms of other constants
		- Build Settings that support constants in _all_ fields
		- user-defined order of items in classes and modules
		- a nicer, dialog-free UI for viewing constants
		
		Until then, this is the best I could come up with. :)
	#tag EndNote


	#tag Constant, Name = AppCreator, Type = String, Dynamic = False, Default = \"rndy", Scope = Public
	#tag EndConstant

	#tag Constant, Name = AppEmail, Type = String, Dynamic = False, Default = \"ryoung@cityofhemet.org", Scope = Public
	#tag EndConstant

	#tag Constant, Name = AppFileName_OSX, Type = String, Dynamic = False, Default = \"HPD Report Tracker", Scope = Public
	#tag EndConstant

	#tag Constant, Name = AppFileName_Windows, Type = String, Dynamic = False, Default = \"HPD Report Tracker.exe", Scope = Public
	#tag EndConstant

	#tag Constant, Name = AppMemory_Minimum, Type = String, Dynamic = False, Default = \"8192", Scope = Public
	#tag EndConstant

	#tag Constant, Name = AppMemory_Suggested, Type = String, Dynamic = False, Default = \"16384", Scope = Public
	#tag EndConstant

	#tag Constant, Name = AppName, Type = String, Dynamic = False, Default = \"HPD Report Tracker", Scope = Public
	#tag EndConstant

	#tag Constant, Name = AppVersion_Short, Type = String, Dynamic = False, Default = \"1.0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = pAppVersion_Long, Type = String, Dynamic = False, Default = \"1.0f001 \xC2\xA92013 Randy Young", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
